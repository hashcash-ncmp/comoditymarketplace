import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CoreDataService } from '../core-data.service';
import { Router } from '@angular/router';
import {ServiceoneService} from '../services/serviceone.service';
@Component({
  selector: 'app-signupotp',
  templateUrl: './signupotp.component.html',
  styleUrls: ['./signupotp.component.css']
})
export class SignupotpComponent implements OnInit {

  public SentOtp:any="";
  public tempMobile: any="";
  public errormessage:any = "";
  public companyId:any="";
  constructor(private _service:ServiceoneService,private http: HttpClient, private data: CoreDataService, private route: Router) { }

  ngOnInit(): void {

    this.tempMobile = localStorage.getItem('tmpMobileno');
    var OTP=localStorage.getItem('tmpOtp');
    alert('Your OTP is  '+OTP);
   // this.companyId= localStorage.getItem('companyId');
  }
  verifyOtp() {
    var otpobj = {};
    otpobj['tempPhNo'] = this.tempMobile;
    otpobj['phNoOtp'] = this.SentOtp;
    
    if(this.tempMobile !="" && this.SentOtp !=""){
      this._service.OTPverify(otpobj)
      .subscribe(response => {
        var result = response;
        if (result.flag== 1) {
          var value = result.userResult;
          // var userId=value.userId;
         // localStorage.setItem('UserId',userId);
         if(value.regProcessStatus==1){
          this.data.alert('You are alreay registered,Please login','success');
         // this.route.navigateByUrl('/');
         window.location.href = "http://52.8.99.117/login";
         }
         else{
          localStorage.setItem('companyId',value.companyId);
          localStorage.setItem('processStep',value.processStep);
          localStorage.setItem('associationflag',value.associationFlag);

          this.route.navigateByUrl('/Signupform');
         }
        
          // localStorage.setItem('companyId',value.companyId);
          // localStorage.setItem('processStep',value.processStep);
          // localStorage.setItem('associationflag',value.associationFlag);

          // this.route.navigateByUrl('/Signupform');
          
        }
        else {
          this.errormessage ='Invalid OTP Provided.';
        }

      })
    }
    else{
      this.errormessage = " Mandatory field missing.";
    }
   
  }
}

import { TestBed } from '@angular/core/testing';

import { PaybitoService } from './paybito.service';

describe('PaybitoService', () => {
  let service: PaybitoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PaybitoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CoreDataService } from '../core-data.service';
import { BehaviorSubject } from 'rxjs';
import * as $ from 'jquery';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';

@Injectable({
  providedIn: 'root'
})
export class PaybitoService {
  //webSocketEndPoint: string =this.data.STREAMWEBSERVICE+ '/FixSocketStream/ws';//'http://184.169.229.227:8080/FixSocketStream/ws';

 // paybitowebSocketEndPoint = 'http://184.72.11.42:10080/marketApi/ws';//http://184.72.11.42:10080/marketApi/ws.
  //topic: string = "/user/topic/stream";
 // stompClient: any;

 // public baseURL = "https://api.paybito.com/api";
  //public sourcebalance:any;
  public paybitoticker = [];
  public paybitoticker1 = [];
  public paybitodemosor = [];
  public offerdata = [];
  public tradedata = [];
  public myBalanceList = [];
  public shortcutasset = [];
  public shortcutsymbol = []
  public reqNo:any;
  // count: BehaviorSubject<number>;
  // tickervalue:BehaviorSubject<any>;
  // private dataSource = new BehaviorSubject(this.paybitoticker);
  // currentData = this.dataSource.asObservable();
  // private dataSource1 = new BehaviorSubject(this.offerdata);
  // currentData1 = this.dataSource1.asObservable();
  // private dataSource2 = new BehaviorSubject(this.tradedata);
  // currentData2 = this.dataSource2.asObservable();
  // private dataSource3 = new BehaviorSubject(this.myBalanceList);
  // currentData3 = this.dataSource3.asObservable();
  // private dataSourceshortcut = new BehaviorSubject(this.shortcutasset);
  // currentDatashortcut = this.dataSourceshortcut.asObservable();
  // private dataSourcesor = new BehaviorSubject(this.paybitodemosor);
  // currentDatasor = this.dataSourceshortcut.asObservable();
  constructor(private http: HttpClient, private data: CoreDataService) {
    //this.allassetsticker();
    // this._connect();
    // setTimeout(() => {
    //   this.sendmessagePAYBITOmarketdata();
    // }, 5000);
  //  this.allassetsortcut();
    //     this.count  = new BehaviorSubject(this.counter);
    // this.tickervalue=new BehaviorSubject(this.shortcutdatapaybito);
  }
  // changeData(data: any) {
  //   this.dataSource.next(data);
  // }


  // allassetsticker() {

  //   this.data.loader = true;
  //   var url = "http://184.169.229.227:8080/brokerFixApi/publish/getAllTickerList";
  //   // var url = "https://stream.paybito.com:8443/BrokerStreamApi/rest/getAllTickerList";

  //   if (this.data.source != undefined) {
  //     this.data.source.close();
  //   }
  //   if (this.data.sourcebalance != undefined) {
  //     this.data.sourcebalance.close();
  //   }
  //   if (this.data.source2 != undefined) {
  //     // alert(this.data.source2);
  //     this.data.source2.close();
  //     //  this.data.source2.unsubscribe();
  //   }
  //   if (this.data.source4 != undefined) {
  //     // alert(this.data.source4);
  //     this.data.source4.close();
  //     // this.data.source4.unsubscribe();
  //   }
  //   this.data.source = new EventSource(url);

  //   var result: any = new Object();
  //   this.data.source.onmessage = (event: MessageEvent) => {
  //     var result = JSON.parse(event.data);
  //     // this.tickerasset = result;
  //     // this.shortcutdata=result;
  //     this.paybitoticker = result;
  //     //  console.log('result--------------', this.paybitoticker);
  //     this.dataSource.next(this.paybitoticker);



  //     this.data.loader = false;
  //     // if (this.tickerasset == null || this.tickerasset == null) {
  //     //   this.nodataMsg = "No Data";

  //     // }

  //   }
  //   // if (this.token.length >= "null" || this.token.length >= 0) {}
  // }
  // _connect() {
  //   //  alert('got it');
  //   console.log("Initialize paybito WebSocket Connection-------------");
  //   let ws = new SockJS(this.webSocketEndPoint);
  //   this.stompClient = Stomp.over(ws);
  //   this.stompClient.debug = null;
  //   const _this = this;
  //   _this.stompClient.connect({}, function (frame) {
  //     _this.stompClient.subscribe(_this.topic, function (sdkEvent) {

  //       _this.onMessageReceive(sdkEvent);
         
  //     });
     
  //     //_this.stompClient.reconnect_delay = 2000;
  //   }, this.errorCallBack);
    
  // };

  // _disconnectPAYBITO() {
  //   if (this.stompClient !== null) {
  //     this.stompClient.disconnect();
  //   }
  //   console.log("Disconnected");
  // }
  // errorCallBack(error) {
  //   console.log("errorCallBack -> " + error)
  //   setTimeout(() => {
  //     this._connect();
  //   }, 5000);
  // }
  // sendmessagePAYBITOmarketdata() {
  //  // this.unsubscribe();
  //   this.reqNo = Math.floor((Math.random() * 1000) + 1);
  //   const req = {
  //    "method":"SUBSCRIBE" ,
  //     "id": this.reqNo,
  //     "params":["ticker"]
  //     };
  //   localStorage.setItem('reqno', this.reqNo);
  //  // localStorage.setItem('symbolcode', this.data.symbolcode);
  //   this._sendrequest(req);
  // }
  // unsubscribe() {
  //   var assettoclose = localStorage.getItem('symbolcode');
  //   var requestnotick = localStorage.getItem('reqno');
  //   const req = {
  //     "method":"UNSUBSCRIBE" ,
  //     "id": requestnotick,
  //     "params":["ticker"]
  //   };

  //   this._sendrequest(req);
  // }
  // _sendrequest(message) {
  //   // alert('sdfsd');
  //   // console.log("calling logout api via web socket");
  //   //console.log(message);
  //   this.stompClient.send("/app/sendRequest", {}, JSON.stringify(message));
  // }
  // onMessageReceive(message) {
  
  //   var str = JSON.stringify(message.body);
  //   var obj = JSON.parse(str);
  //   var marketdata = JSON.parse(obj);
  //   this.paybitoticker1=marketdata.tR;
  // //  console.log("Message Recieved from Server for all tickers :: " + marketdata.tR);
  //   this.dataSource.next(marketdata.tR);
  //  // this.allassetsortcut();
  // }
 
  // allassetsortcut1() {

  //   this.data.loader = true;
  //   var url = "http://184.169.229.227:8080/brokerFixApi/publish/getAllTickerList";
  //   //  var url = "https://stream.paybito.com:8443/BrokerStreamApi/rest/getAllTickerList";

  //   if (this.data.source5 != undefined) {
  //     // alert(this.data.source4);
  //     this.data.source5.close();
  //     // this.data.source4.unsubscribe();
  //   }
  //   this.data.source5 = new EventSource(url);

  //   var result: any = new Object();
  //   this.data.source5.onmessage = (event: MessageEvent) => {
  //     var result = JSON.parse(event.data);

  //     this.paybitoticker1 = result;
  //     // console.log('paybitodemosor--------------', this.paybitodemosor);

  //     if (this.data.tempsearch == undefined && this.shortcutasset.length == 0) {
  //       //   alert(this.data.tempsearch);
  //       for (var i = 0; i <= 9; i++) {
  //         this.shortcutasset.push({ 'crypto': this.paybitoticker1[i].currency, 'base': this.paybitoticker1[i].baseCurrency, 'ltp': this.paybitoticker1[i].ltp, 'lastAskPrice': this.paybitoticker1[i].lastAskPrice, 'lastBidPrice': this.paybitoticker1[i].lastBidPrice, 'roc': this.paybitoticker1[i].roc, 'volume': this.paybitoticker1[i].lastBidPrice, 'action': this.paybitoticker1[i].action });
  //       }

  //     }
  //     else if (this.data.tempsearch != undefined) {

  //       for (var i = 0; i < this.paybitoticker1.length; i++) {

  //         if (this.paybitoticker1[i].currency + this.paybitoticker1[i].baseCurrency == this.data.tempsearch && this.shortcutasset.length <= 10) {
  //           // alert(this.data.tempsearch);
  //           const addedasset = this.shortcutsymbol;
  //           var found = addedasset.includes(this.data.tempsearch);

  //           if (found == false && this.shortcutasset.length == 10) {
  //             // this.shortcutasset.pop();
  //             this.shortcutasset.splice(0, 1, { 'crypto': this.paybitoticker1[i].currency, 'base': this.paybitoticker1[i].baseCurrency, 'ltp': this.paybitoticker1[i].ltp, 'lastAskPrice': this.paybitoticker1[i].lastAskPrice, 'lastBidPrice': this.paybitoticker1[i].lastBidPrice, 'roc': this.paybitoticker1[i].roc, 'volume': this.paybitoticker1[i].lastBidPrice, 'action': this.paybitoticker1[i].action })
  //             // this.shortcutasset.push({'symbol':this.alltickers[i].symbol,'currentClose':this.alltickers[i].currentClose});
  //             this.shortcutsymbol.push(this.paybitoticker1[i].baseCurrency + this.paybitoticker1[i].currency);
  //           }
  //         }
  //       }

  //     }
  //    /// console.log('00000000000000000000000', this.shortcutasset);

  //     this.dataSourceshortcut.next(this.shortcutasset);

  //     this.data.loader = false;
  //     // if (this.tickerasset == null || this.tickerasset == null) {
  //     //   this.nodataMsg = "No Data";

  //     // }

  //   }
  //   // if (this.token.length >= "null" || this.token.length >= 0) {}
  // }
  // allassetsortcut(){

  //   //console.log('-------SOrtcut', this.paybitoticker1);
  //   if (this.data.tempsearch == undefined && this.shortcutasset.length == 0) {
  //     //   alert(this.data.tempsearch);
  //     for (var i = 0; i <= 9; i++) {
  //       this.shortcutasset.push({ 'crypto': this.paybitoticker1[i].currency, 'base': this.paybitoticker1[i].baseCurrency, 'ltp': this.paybitoticker1[i].ltp, 'lastAskPrice': this.paybitoticker1[i].lastAskPrice, 'lastBidPrice': this.paybitoticker1[i].lastBidPrice, 'roc': this.paybitoticker1[i].roc, 'volume': this.paybitoticker1[i].lastBidPrice, 'action': this.paybitoticker1[i].action });
  //     }

  //   }
  //   else if (this.data.tempsearch != undefined) {

  //     for (var i = 0; i < this.paybitoticker1.length; i++) {

  //       if (this.paybitoticker1[i].currency + this.paybitoticker1[i].baseCurrency == this.data.tempsearch && this.shortcutasset.length <= 10) {
  //         // alert(this.data.tempsearch);
  //         const addedasset = this.shortcutsymbol;
  //         var found = addedasset.includes(this.data.tempsearch);

  //         if (found == false && this.shortcutasset.length == 10) {
  //           // this.shortcutasset.pop();
  //           this.shortcutasset.splice(0, 1, { 'crypto': this.paybitoticker1[i].currency, 'base': this.paybitoticker1[i].baseCurrency, 'ltp': this.paybitoticker1[i].ltp, 'lastAskPrice': this.paybitoticker1[i].lastAskPrice, 'lastBidPrice': this.paybitoticker1[i].lastBidPrice, 'roc': this.paybitoticker1[i].roc, 'volume': this.paybitoticker1[i].lastBidPrice, 'action': this.paybitoticker1[i].action })
  //           // this.shortcutasset.push({'symbol':this.alltickers[i].symbol,'currentClose':this.alltickers[i].currentClose});
  //           this.shortcutsymbol.push(this.paybitoticker1[i].baseCurrency + this.paybitoticker1[i].currency);
  //         }
  //       }
  //     }

  //   }
  //  // console.log('00000000000000000000000', this.shortcutasset);

  //   this.dataSourceshortcut.next(this.shortcutasset);

  //   this.data.loader = false;
  // }
  // alloffers(userid, buyingasset, sellingasset) {

  //   this.data.loader = true;
  //   var url =this.data.STREAMWEBSERVICE+ "/brokerFixApi/publish/getOfferByAccountID?userID=" + userid + '&currency=' + buyingasset + '&baseCurrency=' + sellingasset;
   
  //   if (this.data.source != undefined) {
     
  //     this.data.source.close();
  //     // this.dataSource.unsubscribe();
  //   }
  //   if (this.data.sourcebalance != undefined) {
     
  //     this.data.sourcebalance.close();
  //     // this.data.sourcebalance.unsubscribe();
  //   }
  //   if (this.data.source2 != undefined) {
     
  //     this.data.source2.close();
  //     //  this.data.source2.unsubscribe();
  //   }
  //   if (this.data.source4 != undefined) {
  //     // alert(this.data.source4);
  //     this.data.source4.close();
  //     // this.data.source4.unsubscribe();
  //   }
  //   this.data.source2 = new EventSource(url);

  //   var result: any = new Object();
  //   this.data.source2.onmessage = (event: MessageEvent) => {
  //     var result = JSON.parse(event.data);
      
  //     var offerdata = result;
  //     this.offerdata = offerdata['tradeListResult'];
  //   //   console.log('offer--------------dhriti', this.offerdata);
  //     this.dataSource1.next(this.offerdata);

  //     this.data.loader = false;
     

  //   }
    
  // }
  // alltrades(userid, buyingasset, sellingasset, pageno, itemperpage) {
  //   this.data.loader = true;
  //   var url =this.data.STREAMWEBSERVICE + "/brokerFixApi/publish/getTradeHistoryById?userID="+userid+"&currency="+buyingasset+"&baseCurrency="+sellingasset+"&pageNo="+pageno+"&noOfItemsPerPage="+itemperpage;
   
  //   if (this.data.source != undefined) {
  //     this.data.source.close();
  //   }
  //   if (this.data.source2 != undefined) {
  //     this.data.source2.close();
  //   }
  //   if (this.data.source4 != undefined) {
  //     this.data.source4.close();
  //   }
  //   if (this.data.sourcebalance != undefined) {
  //     this.data.sourcebalance.close();
  //   }
  //   this.data.source4 = new EventSource(url);

  //   var result: any = new Object();
  //   this.data.source4.onmessage = (event: MessageEvent) => {
  //     result = JSON.parse(event.data);
  //     // this.tickerasset = result;
  //     // this.shortcutdata=result;
  //     this.tradedata = result.tradeListResult;
  //     this.dataSource2.next(this.tradedata);
  //     //  console.log('trade--------------', this.tradedata);
  //     this.data.loader = false;
     
  //   }
  //   // if (this.token.length >= "null" || this.token.length >= 0) {}
  // }

  // userbalance(userID) {

  //   // this.genarateauthtoken();
  //   //this.data.alert("Loading...", "dark");
  //   this.data.loader = true;
  //   var userTransObj = {};
  //   var userId = localStorage.getItem('user_id');
  //   var url = this.data.WEBSERVICE + "/transaction/getUserBalanceStream?customerId=" + userId;
  //   // this.dataSource.unsubscribe();
  //   // this.data.source2.unsubscribe();
  //   // this.data.source4.unsubscribe();

  //   if (this.data.source != undefined) {
  //     //alert('source');
  //     // this.dataSource.unsubscribe();
  //     this.data.source.close();
  //     //  
  //   }
  //   if (this.data.sourcebalance != undefined) {
  //     // alert(this.data.sourcebalance);
  //     // this.data.sourcebalance.unsubscribe();
  //     this.data.sourcebalance.close();

  //     //
  //   }
  //   if (this.data.source2 != undefined) {
  //     // alert(this.data.source2);
  //     this.data.source2.close();
  //     //  this.data.source2.unsubscribe();
  //   }
  //   if (this.data.source4 != undefined) {
  //     // alert(this.data.source4);
  //     this.data.source4.close();

  //   }
  //   this.data.sourcebalance = new EventSource(url);
  //   var result: any = new Object();
  //   this.data.sourcebalance.onmessage = (event: MessageEvent) => {
  //     // result = event.data;
  //     var result = JSON.parse(event.data);
  //     this.myBalanceList = result.userBalanceList;
  //     this.dataSource3.next(this.myBalanceList);
  //    // console.log('LLLLLLLLLLLLLLLLLLLL', this.myBalanceList);
  //     //alert(this.myBalanceList);
  //     if (this.myBalanceList != null) {
  //       this.data.loader = false;
  //     }

  //   };
  //   // })
  // }
  // Userappsetting(data) {
  //   return this.http.post<any>(this.data.WEBSERVICE + '/user/GetUserAppSettings', data, {
  //     headers: {
  //       'Content-Type': 'application/json',
  //       'authorization': 'BEARER ' + localStorage.getItem('access_token'),
  //     }
  //   })
  // }
  // // nextCount() {
  // //   this.count.next(++ this.counter);
  // // }
  // loginthrouIdpassword(data) {
  //   // return this.http.post<any>(this.baseURL+'BrokerApp/user/login',JSON.stringify(data),{ headers: { 'content-Type': 'application/json' } }); 
  //   return this.http.post<any>(this.data.WEBSERVICE + '/user/LoginWithUsernamePassword', data, { headers: { 'content-Type': 'application/json' } })
  // }
  
  // GetUserbalance(jsonString) {
  //   return this.http.post<any>(this.data.WEBSERVICE + '/transaction/getUserBalance', JSON.stringify(jsonString), {
  //     headers: {
  //       'Content-Type': 'application/json',
  //       'authorization': 'BEARER ' + localStorage.getItem('access_token')
  //     }
  //   })
  // }
  // BuyOffer(data) {
  //   return this.http.post<any>(this.data.WEBSERVICE + '/userTrade/TradeCreateOffer', JSON.stringify(data), {
  //     headers: {
  //       'Content-Type': 'application/json',
  //       'authorization': 'BEARER ' + localStorage.getItem('access_token')
  //     }
  //   })
  //   //  return this.http.post<any>(this.data.WEBSERVICE + '/userTrade/TradecreateOffer', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  // }
  // SellOffer(data) {
  //   return this.http.post<any>(this.data.WEBSERVICE + '/userTrade/TradeCreateOffer', JSON.stringify(data), {
  //     headers: {
  //       'Content-Type': 'application/json',
  //       'authorization': 'BEARER ' + localStorage.getItem('access_token')
  //     }
  //   })
  //   //return this.http.post<any>(this.data.WEBSERVICE + '/userTrade/TradecreateOffer', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });


  // }
  // GetAssets() {
  //   // return  this.http.get<any>('http://184.72.11.42:4495/BrokerApp/publish/getAssetList/PAYBITO');
  //   return this.http.get<any>(this.data.STREAMWEBSERVICE+'/CacheService/api/getData?Name=Assets');
  
  // }
  // Deleteoffer(data) {
  //   return this.http.post<any>(this.data.WEBSERVICE + '/userTrade/TradeManageOffer', data, {
  //     headers: {
  //       'Content-Type': 'application/json',
  //       'authorization': 'BEARER ' + localStorage.getItem('access_token')
  //     }
  //   })
   
  // }
  // Updateofferamount(data) {
  //   return this.http.post<any>(this.data.WEBSERVICE + '/userTrade/TradeManageOffer', data, {
  //     headers: {
  //       'Content-Type': 'application/json',
  //       'authorization': 'BEARER ' + localStorage.getItem('access_token')
  //     }
  //   })
   
  // }
  // Updateofferprice(data) {
  //   return this.http.post<any>(this.data.WEBSERVICE + '/userTrade/TradeManageOffer ', data, {
  //     headers: {
  //       'Content-Type': 'application/json',
  //       'authorization': 'BEARER ' + localStorage.getItem('access_token')
  //     }
  //   })
  
  // }
  // Getofferdata(data) {
  

  //   return this.http.post<any>('http://184.169.229.227:8080/brokerFixApi/publish/getOfferByAccountID', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  // }
  // Getalltrade(data) {
  //   //  http://184.169.229.227:8080/brokerFixApi/publish/getTradeHistoryById?userID=3050&currency=BTC&baseCurrency=USD&pageNo=1&noOfItemsPerPage=10

  //   return this.http.post<any>('http://184.169.229.227:8080/brokerFixApi/publish/getTradeHistoryByI', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });

  // }
  Pricecheck(data){
    return this.http.post<any>(this.data.WEBSERVICE + '/userTrade/OfferPriceCheck', data, {
      headers: {
        'Content-Type': 'application/json',
        'authorization': 'BEARER ' + localStorage.getItem('access_token')
      }
    })
  }
}

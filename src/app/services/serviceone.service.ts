import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Employee } from '../core-data-veriable';  
import{Observable}from 'rxjs';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { CoreDataService } from '../core-data.service';

@Injectable({
  providedIn: 'root'
})
export class ServiceoneService {
  
  constructor(private http: HttpClient,private data:CoreDataService) { 
    
  }


  GetOtp(data) {
       
    return this.http.post<any>(this.data.WEBSERVICE + '/users/insertTempUserDetails',JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  OTPverify(data) {
   
    return this.http.post<any>(this.data.WEBSERVICE + '/users/checkOtp',JSON.stringify(data), { headers: { 'content-Type': 'application/json' } })
  }
  InsertCompanydata(data){
    return this.http.post<any>(this.data.WEBSERVICE + '/users/addCompanyDetails',JSON.stringify(data), { headers: { 'content-Type': 'application/json' } })
  }
  InsertPersonaldata(data){
    return this.http.post<any>(this.data.WEBSERVICE + '/users/addPersonalDetails',data, { headers: { 'content-Type': 'application/json' } })
  }
  InsertAssociationdata(data){
    return this.http.post<any>(this.data.WEBSERVICE + '/users/associationInfo',JSON.stringify(data), { headers: { 'content-Type': 'application/json' } })
  }
  InsertAothorizationdata(data){
    return this.http.post<any>(this.data.WEBSERVICE + '/users/addAuthorizationInfo',JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
Getregtype(){
  return  this.http.get<any>(this.data.WEBSERVICE+'/users/getCompanyRegTypes');
}
GetallInfo(companyId){
 // return  this.http.post<any>(this.data.WEBSERVICE+'/users/getPreviewDetails?'+companyId);
return this.http.post<any>(this.data.WEBSERVICE + "/users/getPreviewDetails",companyId)
}
Updatecompanydetails(data){
  return this.http.post<any>(this.data.WEBSERVICE + '/users/updateCompanyDetails',JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
}
Updatepersonaldetails(data){
  return this.http.post<any>(this.data.WEBSERVICE + '/users/updatePersonalDetails',JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
}
Updateassociationdetails(data){
  return this.http.post<any>(this.data.WEBSERVICE + '/users/updateAssociationInfo',JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
}
Updateaauthorizationdetails(data){
  return this.http.post<any>(this.data.WEBSERVICE + '/users/updateAuthorizationInfo',JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
}
Updatebankdetails(data){
  return this.http.post<any>(this.data.WEBSERVICE + '/users/updateBankDetails',JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
}
getBusinesstype(){
  return  this.http.get<any>(this.data.WEBSERVICE+'/users/getCompanyBusinessTypes');
}
getcompanystatedistrict(){
 // return  this.http.get<any>(this.data.WEBSERVICE+'/location/getLocationListByCountry?countryId=1');
 var countryId=1;
  return this.http.post<any>(this.data.WEBSERVICE + "/location/getLocationListByCountry?countryId=1",'')

}
CheckPhoneForConpanyInfo(data){
  return this.http.post<any>(this.data.WEBSERVICE + '/users/checkPhone',data, { headers: { 'content-Type': 'application/json' } });
}
CheckEmailForCompanyEmail(data){
  return this.http.post<any>(this.data.WEBSERVICE + '/users/checkEmail',data, { headers: { 'content-Type': 'application/json' } });
}
CheckphoneForAuthorisedInfo(data){
  return this.http.post<any>(this.data.WEBSERVICE + '/users/checkAuthPhone',data, { headers: { 'content-Type': 'application/json' } });
}
CheckEmailForAuthorisedEmail(data){
  return this.http.post<any>(this.data.WEBSERVICE + '/users/checkAuthEmail',data, { headers: { 'content-Type': 'application/json' } });
}
CheckGst(data){
  return this.http.post<any>(this.data.WEBSERVICE + '/users/checkGst',data, { headers: { 'content-Type': 'application/json' } });
}
CheckPan(data){
  return this.http.post<any>(this.data.WEBSERVICE + '/users/checkPan',data, { headers: { 'content-Type': 'application/json' } });
}
}

import { TestBed } from '@angular/core/testing';

import { ServicethreeService } from './servicethree.service';

describe('ServicethreeService', () => {
  let service: ServicethreeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServicethreeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

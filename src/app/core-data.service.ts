import { Injectable } from '@angular/core';
import * as $ from 'jquery';
import { HttpClient, HttpHeaders } from '@angular/common/http';
//import { AnyARecord } from 'dns';

@Injectable({
  providedIn: 'root'
})
export class CoreDataService {
  reason: any;
  icon: any;
   loader:boolean=false;
//   SelectedBuyasset:any;
//   SelectedSellasset:any;
//   Lasttraderprice:any;
//   source:any;
//   courceConn:any;
//   source2:any;
//   source4:any;
//   sourcebalance:any;
//  sourse3:any;
//  source5:any
//  symbol:any;
  WEBSERVICE:any;
 TRADESERVICE:any;
//  symbolcode:any;
//  tempsearch:any;
//  Pprecision:any;
//  Aprecision:any;
//  draggable:boolean=true;
  STREAMWEBSERVICE:any;
 // baseURL='http://184.72.11.42:4495/';
  constructor(private http: HttpClient) {
    var alertPl = `<div class="alertPlace"></div>`;
    $('html').append(alertPl).fadeIn();
     this.WEBSERVICE='http://54.177.218.175:8080/crmFoApi';
    // this.STREAMWEBSERVICE='https://fixapi.paybito.com:8443';//'http://184.169.229.227:8080/api';
    // this.TRADESERVICE = 'https://fixapi.paybito.com:8443/TradeAmount/rest/MyAmount';//"http://184.169.229.227:8080/TradeAmount/rest/MyAmount";
    //this.CHARTSERVISE = "http://184.169.229.227:8080/TrendSpriceVolume/paybito/";//live
    // this.REPORTSERVISE = "http://184.169.229.227:8080/PaybitoReportModule/report/";
  //  this.WEBSERVICE='http://184.72.11.42:7503/api';
   // this.WEBSERVICE= 'http://fixapi.hashcashconsultants.com:8080/api';

   }

  alert(msg, type, time = 3000) {
    //console.log(msg, type);
 
    this.reason = msg;
    this.icon = 'puff';

    if (msg == 'Loading...') {
      this.loader = true;
      setTimeout(() => {
        this.loader = false;
      }, 6000);
    } else {
      $('.alert:first').fadeOut();
      var htx = `<div class="alert alert-` + type + ` my-2" role="alert">` + msg + `</div>`;
      $('.alertPlace').append(htx).fadeIn();
     // console.log('/////////////////////////////',htx);
      setTimeout(() => {
        $('.alert:last').remove().fadeOut();
      }, time);
    }
  }
 
  //http://184.72.11.42:7503/BrokerApp/user/login
}

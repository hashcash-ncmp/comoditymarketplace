import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import{NavbarComponent} from "../app/navbar/navbar.component";

import{LayoutComponent} from "../app/layout/layout.component";
import{LoginComponent} from "../app/login/login.component";

import{TvChartContainerComponent} from "../app/tv-chart-container/tv-chart-container.component";

import { ForgetpasswordComponent } from './forgetpassword/forgetpassword.component';
import { SecuretokenComponent } from './securetoken/securetoken.component';
import { RegistrationOneComponent } from './registration-one/registration-one.component';
import { SignupformComponent } from './signupform/signupform.component';
import { SignupotpComponent } from './signupotp/signupotp.component';
import { AccountinfoComponent } from './accountinfo/accountinfo.component';
const routes: Routes = [
  { path: 'test', component: NavbarComponent },
 
  { path: 'layout', component: LayoutComponent },
  // { path: '', component: LoginComponent },
  { path: '', component: RegistrationOneComponent },
  { path: 'Signupform', component: SignupformComponent },
  { path: 'Signupotp', component: SignupotpComponent },
  { path: 'Accountinfo', component: AccountinfoComponent },
  { path: 'chart', component: TvChartContainerComponent },
  
  { path: 'forgetpassword', component: ForgetpasswordComponent },
  { path: 'stoken', component: SecuretokenComponent },
  // { path: 'marketdata', loadChildren: () => import('./marketdata/marketdata.module').then(m => m.MarketdataModule) }
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { CoreDataService } from '../core-data.service';
import { HttpClient } from '@angular/common/http';

import { Router } from '@angular/router';
@Component({
  selector: 'app-forgetpassword',
  templateUrl: './forgetpassword.component.html',
  styleUrls: ['./forgetpassword.component.css']
})
export class ForgetpasswordComponent implements OnInit {
  forgetpwdObj:any={};
  constructor(private data: CoreDataService,private http:HttpClient,private route:Router ) { }

  ngOnInit(): void {
  }

  confirmPassword(e) {
    if (
      (this.forgetpwdObj.password != '' && this.forgetpwdObj.password != undefined) &&
      (this.forgetpwdObj.repassword1 != '' && this.forgetpwdObj.repassword1 != undefined)
    ) {
      if (this.forgetpwdObj.password == this.forgetpwdObj.repassword1) {
        $('.confirm_password_text').html('Password Matched');
        $('.confirm_password_text').css('color', 'lightgreen');
        $('#submit_btn').removeAttr('disabled');
      } else {
        $('.confirm_password_text').html('Password  Mismatched');
        $('.confirm_password_text').css('color', 'red');
        $('#submit_btn').attr('disabled', 'disabled');
      }
    } else {
  
    }
  
  }
  checkPassword() {
    var password = this.forgetpwdObj.password;
    if (password != '' && password != undefined && password.length >= 8) {
      var passwordStatus = this.checkAlphaNumeric(password);
      if (passwordStatus == false) {
        this.data.alert('Password should have atleast one upper case letter, one lowercase letter , one special case and one number', 'warning');
      } else {
      }
    }
    else {
      this.data.alert('Password should be minimum 8 Charecter', 'warning');
    }
  }
 
  checkAlphaNumeric(string) {
    if (string.match(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/)) {
      return true;
    } else {
      return false;
    }
  }
  forgotPassword(isValid){

    if(isValid){
      var jsonString=JSON.stringify(this.forgetpwdObj);
      this.http.post<any>(this.data.WEBSERVICE+'/user/ResetPassword',jsonString,{headers: {
          'Content-Type': 'application/json'
        }})
     .subscribe(response=>{
       //  wip(0);
        var result=response;
        if(result.error.error_data!='0'){
          this.data.alert(result.error.error_msg,'danger');
        }else{
 
        this.data.alert('Password successfully reset','success');
        this.route.navigateByUrl('/');
 
        }
 
      },function(reason){
       //  wip(0);
        this.data.alert('Internal Server Error','danger')
      });
    }else{
     //$('.submit_btn').attr('disabled','disabled');
      this.data.alert('Please provide valid email','warning');
    }
  }
}

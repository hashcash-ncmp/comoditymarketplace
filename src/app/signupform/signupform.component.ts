import { Component, OnInit } from '@angular/core';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import * as $ from 'jquery';
import { Router } from '@angular/router';

import {
  NgbModal
} from '@ng-bootstrap/ng-bootstrap';
import { HttpClient } from '@angular/common/http';
import { ServiceoneService } from '../../app/services/serviceone.service';
import { CoreDataService } from '../core-data.service';
import { AppRoutingModule } from '../app-routing.module';
import { IfStmt } from '@angular/compiler';
import { DatePipe } from '@angular/common';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';


@Component({
  selector: 'app-signupform',
  templateUrl: './signupform.component.html',
  styleUrls: ['./signupform.component.css']
})
export class SignupformComponent implements OnInit {
  // model: NgbDateStruct;
  // model1:NgbDateStruct;

  public countries = [];
  public states = [];
  public statearray = [];
  public zones = [];
  public zonearray = [];
  public zonesPone = [];
  public zonesPtwo = [];
  public zonesPthree = [];
  public zonesAuth = [];
  public zonearrayP = [];
  public City = [];
  public cityarray = [];
  public CityPone = [];
  public CityPtwo = [];
  public CityPthree = [];
  public cityarrayP = [];
  public CityAuth = [];
  public showApplicantInfo: boolean = true;
  public isApplicantInfoActive: boolean = true;
  public showOfficeAddressInfo: boolean = false;

  // public isOfficeAddressInfoActive: boolean = true;

  // public isPersonalInfoActive: boolean = true;

  // public isBankInfoActive: boolean = true;

  //-------------------
  public showCompanyInfo: boolean = true;
  public showPersonalInfo: boolean = false;
  public showassociationInfo: boolean = false;
  public showaothrizationInfo: boolean = false;
  //rupam
  // public showPersonalInfo: boolean = false;
  public showKycInfo: boolean = false;
  public showmembershipInfo: boolean = false;
  public showBankInfo: boolean = false;
  public showAssociationInfo: boolean = false;
  //-------------------
  public isAssociationInfoActive: boolean = true;
  public showDisclosureInfo: boolean = false;

  public bankingInfo: boolean = false;
  public showpreviewInfo: boolean = false;
  // public isDisclosureInfoActive: boolean = true;
  // -----
  currentDate: any = new Date().toISOString().slice(0, 10);
  currentYear: any = new Date().getFullYear();
  currentMonth: any = (new Date().getMonth()) + 1;
  currentDay: any = new Date().getDate();
  public allbusinessType = [];
  public regType: any;
  public entityName: any = '';
  public entityType: string = '';
  public websitename: any = '';
  public businessType: string = '';
  public tempval: any = '';
  public comAddressone: any = '';
  public comAddress2: any = '';
  public comCountry: any = 1;
  public comState: string = '';
  public comDistrict: any = '';
  public comCity: any = '';
  public comPin: any = '';
  public comEmail: any = '';
  public companNo: any = '';
  public comgstNo: any = '';
  public comIec: any = '';
  public comCin: any = '';
  public PanIssueDate: any;
  public GstIssueDate: any;
  public IncorporationDate: any;
  public comContact: any;
  public Cinshow: boolean = true;
  public gsterror: any = "";
  //----------correspondence
  public AgreekCopyAddress: any
  public corrAddressone: any;
  public corrAddress2: any;
  public corrCountry: any = 1;
  public corrState: string = '';
  public corrDistrict: string = '';
  public corrCity: any;
  public corrPin: any

  //--------
  // -----
  public Ppincode: any;
  public pCity: any;
  public Pdistrict: any;
  public personalInfoID: any;
  public pState: string = '';
  public pCountry: any = 1;
  public Paddresstwo: any;
  public pAddressone: any;
  public pName: any;
  public pFastname: any;
  public pLaststname: any;
  public Pdesignation: string = "";
  public pContactNo: any;
  public pemailId: any;
  public PpanNo: any;
  public PadharNo: any;
  public PappointedDate: any;
  public personalInfodata = [];
  public personalUpdateInfodata = [];
  public personalInfo: any[] = [{
    id: 1,
    companyId: localStorage.getItem('companyId'),
    lastName: '',
    firstName: '',
    designation: '',
    contact: '',
    email: '',
    panNo: '',
    aadharNo: '',
    appointedDate: '',
    addressLineOne: '',
    addressLineTwo: '',
    country: 1,
    state: '',
    district: '',
    city: '',
    pinCode: ''
  }];
  public companyId: any;
  dropdownSettings = {};
  EdropdownSettings = {};
  // public personalInfo: any[] = [{
  //   pFastname: '',
  //   pLaststname: '',
  //   Pdesignation: '',
  //   pContactNo: '',
  //   pemailId: '',
  //   PpanNo: '',
  //   PadharNo: '',
  //   PappointedDate: '',
  //   pAddressone: '',
  //   Paddresstwo: '',
  //   pCountry: '',
  //   pState: '',
  //   Pdistrict: '',
  //   pCity: '',
  //   Ppincode: '',
  // }];

  // --------
  noOfPersonOwner1;
  SSNPassportOwner1; CorporateIdOwner1;
  noOfPersonOwner2;
  CorporateIdOwner;
  noOfPersonOwner3;
  SSNPassportOwner3;
  CorporateIdOwner2;
  CorporateIdOwner3;
  noOfPersonOwner4;
  SSNPassportOwner4;
  CorporateIdOwner4;
  noOfPersonAssociate;
  SSNPassportAssociate;
  CorporateIdAssociate;
  noOfPersonPrincipleOfficer;
  SSNPassportPrincipleOfficer;
  CorporateIdPrincipleOfficer;
  SSNPassportOwner2;

  noOfPersonEntities;
  SSNPassportEntities;
  CorporateIdEntities;
  public associationInfo = [];


  // -------------------Auth
  public aFastname: any;;
  public aLaststname: any;
  public Adistrict: string = '';
  public aDesignation: string = '';
  public aPanNo: any;
  public AadharNumber: any;
  public Apincode: any;
  public aContactNo: any;
  public aEmailId: any;
  public AappointedDate: any;
  public Aaddressone: any;
  public Aaddresstwo: any;
  public Acountry: any = 1;
  public Astate: string = '';
  //public Adistrict: any;
  public Acity: any;
  public Apincode: any;
  public AuthPolicy: string = '';
  //............
  public companydetails: any;
  public paersonaldetails: any;
  public authorizationInfo: any;
  public association: any;
  public bankdetails: any;
  public temppan: any;
  //-------------------------
  public iecCode: any;
  public designation: any;
  public identityProof: File = null;
  public incorporatinarticle: File = null;
  public regRegion: any;
  public regZoneDist: any;
  public regPinCode: any;
  corresAddress1: any;
  corresAddress2: any;
  corresCity: any;
  corresState: any;
  corresCountry: any;
  corresRegion: any
  public corresZoneDist: any;
  public corresPinCode: any;
  public countryApi: any;
  public companyInfo: boolean = true;
  public authentityInfo: boolean = true;
  public applicantName;
  // -----
  /**** defined by @Rupam ****/
  public identityFile: any;
  public companyAddressFile: any;
  public cancelChequeFile: any;
  public letterOfBank: any;
  public authorizedPersonIdentityFile: any;
  public authorizedPersonAddressFile: any;
  public authorizedPersonPicFile: any;
  public authorizedPersonSignatureFile: any;
  public authorizedPersonLetterOfauthorityFile: any;
  public showAuthBankInfo: boolean = false;
  public bankName: any;
  public accHolderName: any;
  public bankAccountType: any = '';
  public bankAccountNo: any;
  public bankBranchName: any;
  public bankIfscCode: any;
  public showMembershipInfo: boolean = false
  public showTermsCondition: boolean = false
  public isPlanSelected: boolean = false;
  public selectedPackage: any = "";
  public privacyPolicy: boolean = false;
  public disclosureAgreement: boolean = false;
  public terms: boolean = false;
  public termsAndConditionError: string = '';
  public selectedUserId: any = '1';
  public companyId: any = localStorage.getItem('companyId');
  public WEBSERVICE: string = 'http://54.177.218.175:8080/crmFoApi'
  public showCompanyIdDetails: boolean = true;
  public showCompanyAddressDetails: boolean = false;
  public showCompanyCancelCheckDetails: boolean = false;
  public showCompanyLOBDetails: boolean = false;
  public authPersonKycIdList: Array<string> = [];
  public authCompanyKycIdList: Array<string> = []
  public companyKycDocList: Array<string> = []
  public selectAuthPersonIdentityId: string = ''
  public selectAuthPersonIdentityAddress: string = '';
  public membershipPlanList: Array<string> = []
  public companyKycDocArray: any = []
  public selectedPlanDesc: string = '';
  public selectedPlanFee: string = '';
  public selectedPlanDuration: string = '';
  public selectedPlanTradingFee: string = '';
  public selectedPlanTradingLimit: string = '';
  public invoiceDescription: string = '';
  public invoicePurposeCode: string = '';
  public invoiceMemberShipType: string = '';
  public invoiceBankBeneficiaryName: string = '';
  public invoiceAmount: string = '';
  public invoiceGST: string = '';
  public invoiceBankName: string = '';
  public invoiceBankAccountNo: string = '';
  public invoiceBankIfscCode: string = '';
  public invoiceBankAccountType: string = '';
  public invoiceId: string = '';
  //  EDIT
  public Ecomdistrict: string = '';
  public EentityName: any = '';
  public EentityType: string = '';
  public Ewebsitename: any = '';
  public EbusinessType = [];
  public EbusinessTypestring: any = '';
  public EcomAddressone: any = '';
  public EcomAddress2: any = '';
  public EcomCountry: any = 1;
  public EcomState: string = '';
  public EcomDistrict: any = '';
  public EcomCity: any = '';
  public EcomPin: any = '';
  public EcomEmail: any = '';
  public EcompanNo: any = '';
  public EcomgstNo: any = '';
  public EcomIec: any = '';
  public EcomCin: any = '';
  public EPanIssueDate: any;
  public EGstIssueDate: any;
  public EIncorporationDate: any;
  public EcomContact: any;
  public EcorrPin: any;
  public EcorrCity: any;
  public EcorrDistrict: any;
  public EcorrState: string = '';
  public EcorrCountry: any = 1;
  public EcorrAddressone: any;
  public EcorrAddress2: any;
  public datePickerDisabled = true;
  public PandatePickerDisabled = true;
  public CindatePickerDisabled = true;
  public datePickerDisabledAuth = true;
  public EAppointedDateper = true;
  public previewone = true;
  public previewtwo = true;
  public previewthree = true;
  public previewfour = true;
  //public previewfive = true;
  //public previewsix = true;
  public previewseven = true;
  public previeweight = true;
  public previewnine = true;
  public previewten = true;
  public previeweleven = true;
  public previewtwelve = true;
  public previewthirteen = true;
  public previewfourteen = true;
  public previewfifteen = true;
 // public previewsixteen = true;
 // public previewseventeen = true;
  public previeweighteen = true;
  public previewnineteen = true;
  public previewtwenty = true;
  public previewtwentyone = true;
  public previewtwentytwo = true;
  public previewtwentythree = true;
  public previewtwentyfour = true;
  public previewtwentyfive = true;
  public previewPersonalone = true;
  public previewPersonaltwo = true;
  public previewPersonalthree = true;
  public previewPersonalfour = true;
  public previewPersonalfive = true;
  public previewPersonalsix = true;
  public previewPersonalseven = true;
  public previewPersonaleight = true;
  public previewPersonalnine = true;
  public previewPersonalten = true;
  public previewPersonaleleven = true;
  public previewPersonaltwelve = true;
  public previewPersonalthirteen = true;
  //---------
  public previewPersonalonearrayone = true;
  //----------
  public previewauthone = true;
  public previewauthtwo = true;
  public previewauththree = true;
  // public previewauthfour = true;
  // public previewauthfive = true;
  public previewauthsix = true;
  public previewauthseven = true;
  public previewautheight = true;
  public previewauthnine = true;
  public previewauthten = true;
  public previewautheleven = true;
  public previewauthtwelve = true;
  public previewauththirteen = true;
  public previewauthfourteen = true;
  public previewbankone = true;
  public previewbanktwo = true;
  public previewbankthree = true;
  public previewbankfour = true;


  //-------------
  public EPpincode: any;
  public EpCity: string = '';
  public EPdistrict: string = '';
  public EpState: string = '';
  public EpCountry: any = 1;
  public EPaddresstwo: any;
  public EpAddressone: any;
  public EpFastname: any;
  public EpLaststname: any;
  public EPdesignation: string = "";
  public EpContactNo: any;
  public EpemailId: any;
  public EPpanNo: any;
  public EPadharNo: any;
  public EAppointedDateP: NgbDateStruct;
  //---------
  // public EPpincodeTwo: any;
  public EpCityTwo: string = '';
  public EcomDistrictTwo: string = '';
  public EpStateTwo: string = '';
  public EpCountryTwo: any = 1;
  public EPaddresstwoTwo: any;
  public EpAddressoneTwo: any;
  public EpFastnameTwo: any;
  public EpLaststnameTwo: any;
  public EPdesignationTwo: string = "";
  public EpContactNoTwo: any;
  public EpemailIdTwo: any;
  public EPpanNoTwo: any;
  public EPadharNoTwo: any;
  public EPpincodeTwo: any;
  public EAppointedDatePTwo: NgbDateStruct;
  //--------------
  public EPpincodeThree: any;
  public EpCityThree: any;
  public EcomDistrictThree: any;
  public EpStateThree: string = '';
  public EpCountryThree: string = '';
  public EPaddresstwoThree: any;
  public EpAddressoneThree: any;
  public EpFastnameThree: any;
  public EpLaststnameThree: any;
  public EPdesignationThree: string = "";
  public EpContactNoThree: any;
  public EpemailIdThree: any;
  public EPpanNoThree: any;
  public EPadharNoThree: any;
  public EPpincodeThree: any;
  public EAppointedDatePThree: NgbDateStruct;
  //-----------
  public EPpincodeThree: any;
  public EpCityThree: any;
  public EPdistrictThree: any;
  public EpStateThree: string = '';
  public EpCountryThree: string = '';
  public EPaddresstwoThree: any;
  public EpAddressoneThree: any;
  public EpFastnameThree: any;
  public EpLaststnameThree: any;
  public EPdesignationThree: string = "";
  public EpContactNoThree: any;
  public EpemailIdThree: any;
  public EPpanNoThree: any;
  public EPadharNoThree: any;
  public EPpincodeThree: any;
  public EAppointedDatePThree: NgbDateStruct;
  public EpersonalInfodata = [];
  //-----edit association
  public previewAssociation = true;
  EAssocicategoty;
  EAssociname;
  EAssocipassport;
  EAssocicorporateId;
  public EAid: any;
  public updateAssociation = [];
  public EAssociationEdit = false;
  public EAssociationSubmit = false;
  //-------------edit authorization
  public EaFastname: any;;
  public EaLaststname: any;
  public EAdistrict: string;
  public EaDesignation: string = '';
  public EaPanNo: any;
  EAadharNumber;
  public EApincode: any;
  EaContactNo;;
  EaEmailId;

  EAaddressone;
  EAaddresstwo;
  //public Acountry: string = '';
  public Astate: string = '';
  public EAstate: string = '';
  //EAdistrict;
  EAcity;
  public EApincode: any;
  public EAcountry: any = 1;
  public AuthPolicy: string = '';
  public AuthId: any;
  //---------------
  public EaccHolderName: any;
  public EBankName: any;
  public EBankAccountType: any = '';
  public EbankBranchName: any;
  public EbankIfscCode: any;
  public EBankAccountNo: any;
  public bankId: any;
  public EPanNoIssueDate: NgbDateStruct;
  public EGstNoIssueDate: NgbDateStruct;
  public EInNocorporationDate: NgbDateStruct;
  public EAppointedDate: NgbDateStruct;
  public EAappointedDate: NgbDateStruct;
  public tempMobile: any;
  // public personal: any[] = [{
  //   id:1,
  //   address: '',
  //   street: '',
  //   city: '',
  //   country: ''
  // }];

  //= { year: 2021, month: 7, day: 14 }; // July, 14 1789
  constructor(private rout: Router, private datePipe: DatePipe, private data: CoreDataService, private modalService: NgbModal, private http: HttpClient, private _Service: ServiceoneService, private _route: Router) { }

  ngOnInit(): void {
    // this.IncorporationDate = '{year:2021,month:01,day:20}';
    this.tempMobile = localStorage.getItem('tmpMobileno');
    // alert(this.tempMobile);
    if (this.tempMobile == "" || this.tempMobile == undefined) {

      this.rout.navigateByUrl('/');
    }
    else {
      // this.comCountry = 'India';
      // this.corrCountry = 'India';
      this.companyId = localStorage.getItem('companyId');
      var Processstep = localStorage.getItem('processStep');
      var associatonFlag = localStorage.getItem('associationflag');
      if (Processstep == '0') {
        this.showCompanyInfo = true;
        this.showassociationInfo = false;
        this.showPersonalInfo = false;
        this.showaothrizationInfo = false;
        this.showKycInfo = false;
        this.showAuthBankInfo = false;
        this.showMembershipInfo = false;
        this.showTermsCondition = false;
        this.showpreviewInfo = false;
      }
      else if (Processstep == '1') {
        this.showCompanyInfo = false;
        this.showassociationInfo = false;
        this.showPersonalInfo = true;
        this.showaothrizationInfo = false;
        this.showKycInfo = false;
        this.showAuthBankInfo = false;
        this.showMembershipInfo = false;
        this.showTermsCondition = false;
        this.showpreviewInfo = false;
      }
      else if (Processstep == '2' && associatonFlag == 'Y') {
        this.showCompanyInfo = false;
        this.showassociationInfo = true;
        this.showPersonalInfo = false;
        this.showaothrizationInfo = false;
        this.showKycInfo = false;
        this.showAuthBankInfo = false;
        this.showMembershipInfo = false;
        this.showTermsCondition = false;
        this.showpreviewInfo = false;
      }
      else if (Processstep == '2' && associatonFlag != 'Y') {
        this.showCompanyInfo = false;
        this.showassociationInfo = false;
        this.showPersonalInfo = false;
        this.showaothrizationInfo = true;
        this.showKycInfo = false;
        this.showAuthBankInfo = false;
        this.showMembershipInfo = false;
        this.showTermsCondition = false;
        this.showpreviewInfo = false;
      }
      else if (Processstep == '3') {
        this.showCompanyInfo = false;
        this.showassociationInfo = false;
        this.showPersonalInfo = false;
        this.showaothrizationInfo = true;
        this.showKycInfo = false;
        this.showAuthBankInfo = false;
        this.showMembershipInfo = false;
        this.showTermsCondition = false;
        this.showpreviewInfo = false;
      }
      else if (Processstep == '4') {
        this.showCompanyInfo = false;
        this.showassociationInfo = false;
        this.showPersonalInfo = false;
        this.showaothrizationInfo = false;
        this.showKycInfo = false;
        this.showAuthBankInfo = true;
        this.showMembershipInfo = false;
        this.showTermsCondition = false;
        this.showpreviewInfo = false;
      }
      else if (Processstep == '5') {
        this.showCompanyInfo = false;
        this.showassociationInfo = false;
        this.showPersonalInfo = false;
        this.showaothrizationInfo = false;
        this.showKycInfo = true;
        this.showAuthBankInfo = false;
        this.showMembershipInfo = false;
        this.showTermsCondition = false;
        this.showpreviewInfo = false;
        this.renderKycDocInfo();
      }
      else if (Processstep == '6') {
        this.getpreviewInfo();
        this.showCompanyInfo = false;
        this.showassociationInfo = false;
        this.showPersonalInfo = false;
        this.showaothrizationInfo = false;
        this.showKycInfo = false;
        this.showAuthBankInfo = false;
        this.showMembershipInfo = false;
        this.showTermsCondition = false;
        this.showpreviewInfo = true;

      }
      else if (Processstep == '7') {
        this.showCompanyInfo = false;
        this.showassociationInfo = false;
        this.showPersonalInfo = false;
        this.showaothrizationInfo = false;
        this.showKycInfo = false;
        this.showAuthBankInfo = false;
        this.showMembershipInfo = false;
        this.showTermsCondition = true;
        this.showpreviewInfo = false;
      }
      else if (Processstep == '8') {
        this.showCompanyInfo = false;
        this.showassociationInfo = false;
        this.showPersonalInfo = false;
        this.showaothrizationInfo = false;
        this.showKycInfo = false;
        this.showAuthBankInfo = false;
        this.showMembershipInfo = true;
        this.showTermsCondition = false;
        this.showpreviewInfo = false;
        this.getAuthorizationEmailforInvoice();

      }
      else if (Processstep == '9') {

        this.data.alert('You are already registered,Please login', 'success');
        // this._route.navigateByUrl('/');

        window.location.href = "http://52.8.99.117/login";
        this.showCompanyInfo = false;
        this.showassociationInfo = false;
        this.showPersonalInfo = false;
        this.showaothrizationInfo = false;
        this.showKycInfo = false;
        this.showAuthBankInfo = false;
        this.showMembershipInfo = false;
        this.showTermsCondition = false;
        this.showpreviewInfo = false;
      }

    }

    // this.getcountry();
    this.Getstate();
    /**** Method called by @Rupam*****/
    /*** Method calling for rendering KYC fields****/

    /*** Method calling for rendering KYC fields for authorized person****/
    this.renderAuthPersonKycDocInfo();
    this.renderAuthCompanyKycDocInfo();
    /*** Method calling for rendering membership plan dropdown options****/
    this.renderMembershipPlans();
    this.getregistrationtype();
    this.getbusinesstype();
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'businessTypeId',
      textField: 'businessTypeName',
      unSelectAllText: 'UnSelect All',
      enableCheckAll: false,
      itemsShowLimit: 3,
      allowSearchFilter: false
    };
  }


  onItemSelect(item: any) {
    debugger;
    console.log(this.businessType);
    if (this.tempval == '') {
      this.tempval = item.businessTypeId;
    }
    else {
      this.tempval = item.businessTypeId + ',' + this.tempval;
    }

  }
  onSelectAll(items: any) {
    console.log(items);

  }
  onItemDeSelect(item: any) {
    debugger;
    console.log('', item);
    var temp = [];
    temp = this.tempval.split(',');
    for (var i = 0; i < temp.length; i++) {
      var bId = item.businessTypeId.toString();
      if (temp[i] == bId) {
        var found = temp.includes(bId);
        if (found == true) {
          temp.splice(i, 1);
        }
      }


      this.tempval = temp.toString();
      console.log(this.tempval);

    }


  }

  getCinflag(flag) {
    //  alert(flag);
    var tempval = flag;
    if (tempval == 7 || tempval == 2 || tempval == 3) {
      this.Cinshow = true;
    }
    else {
      this.Cinshow = false;
    }

  }
  addAddress() {
    if (this.personalInfo.length <= 2) {
      this.personalInfo.push({
        id: this.personalInfo.length + 1,
        companyId: localStorage.getItem('companyId'),
        lastName: '',
        firstName: '',
        designation: '',
        contact: '',
        email: '',
        panNo: '',
        aadharNo: '',
        appointedDate: '',
        addressLineOne: '',
        addressLineTwo: '',
        country: 1,
        state: '',
        district: '',
        city: '',
        pinCode: ''
      });
    }

  }
  removeAddress(i: number) {
    this.personalInfo.splice(i, 1);
  }

  // handleCompanyInfo() {
  //   this.companyInfo = true;
  // }


  // getcountry() {
  //   //debugger;
  //   this.countryApi = this.http.get<any>('./assets/data/countrystate_new.json')
  //     .subscribe(data => {
  //       this.countries = data;
  //       // console.log('______',this.countries.length);

  //       // this.states=data.states;
  //     })
  //   setTimeout(() => this.Getstate('India'), 1000);

  // }

  // Getstate(data) {
  //   // var temparrray = this.countries;
  //   // for (var i = 0; i < this.countries.length; i++) {
  //   //   if (this.countries[i].Country == data) {
  //   //     var found = this.states.includes(this.countries[i].State);
  //   //     if (found == false) {
  //   //       this.states.push(this.countries[i].State);
  //   //     }

  //   //   }
  //   // }
  //   // console.log('-------------', this.states);

  // }
  Getstate() {
    this.zones = [];
    this.City = [];
    this.states = [];
    this.statearray = [];
    this.statearray = [];
    this.comState="";
    this._Service.getcompanystatedistrict()
      .subscribe(data => {
        this.countries = data.locationList;
        //var temparrray = this.countries;
        //  console.log('-------------', this.countries);
        for (var i = 0; i < this.countries.length; i++) {
          if (this.countries != null) {

            var found = this.statearray.includes(this.countries[i].stateId);
            if (found == false) {
              this.states.push({ 'stateName': this.countries[i].stateName, 'stateId': this.countries[i].stateId });
            }
            this.statearray.push(this.countries[i].stateId)
          }
        }
        // console.log('-------------', this.states);
        // this.states=data.states;
      })

  }
  GetZone(data) {
    debugger;
    this.CheckeStatecode(data);
    //alert(data);
    this.zones = [];
    this.City = [];
    this.zonearray = [];
    for (var j = 0; j < this.countries.length; j++) {
      if (this.countries[j].stateId == data) {
        var foundzone = this.zonearray.includes(this.countries[j].districtId);
        if (foundzone == false) {
          this.zones.push({ 'districtName': this.countries[j].districtName, 'districtId': this.countries[j].districtId });
        }
        this.zonearray.push(this.countries[j].districtId);
      }
    }
    // console.log('-------------', this.zones);
  }
  Getcity(data) {

    this.City = [];
    this.cityarray = [];
    for (var j = 0; j < this.countries.length; j++) {
      if (this.countries[j].districtId == data) {
        var foundcity = this.cityarray.includes(this.countries[j].cityId);
        if (foundcity == false) {
          this.City.push({ 'cityName': this.countries[j].cityName, 'cityId': this.countries[j].cityId });
        }
        this.cityarray.push(this.countries[j].cityId);
      }
    }
    console.log('-------------', this.City);
  }
  //   GetZoneP(data) {
  //     debugger;
  //     //alert(data);
  //     this.zonesP = [];
  //     for (var j = 0; j < this.countries.length; j++) {
  //       if (this.countries[j].stateId == data) {
  //         var foundzone = this.zonearrayP.includes(this.countries[j].districtId);
  //         if (foundzone == false) {
  //           this.zonesP.push({'districtName':this.countries[j].districtName,'districtId':this.countries[j].districtId});
  //         }
  //         this.zonearrayP.push(this.countries[j].districtId);
  //       }
  //     }
  //     console.log('-------------', this.zones);
  //   }
  //   GetcityP(data){
  //      debugger;
  // this.CityP=[];
  // for (var j = 0; j < this.countries.length; j++) {
  //   if (this.countries[j].districtId == data) {
  //     var foundcity = this.cityarrayP.includes(this.countries[j].cityId);
  //     if (foundcity == false) {
  //       this.CityP.push({'cityName':this.countries[j].cityName,'cityId':this.countries[j].cityId});
  //     }
  //   this.cityarrayP.push(this.countries[j].cityId);
  //   }
  // }

  //   }
  GetZoneP1(data) {

    this.CityPone = [];
    this.zonearrayP = [];
    this.zonesPone = [];
    for (var j = 0; j < this.countries.length; j++) {
      if (this.countries[j].stateId == data) {
        var foundzone = this.zonearrayP.includes(this.countries[j].districtId);
        if (foundzone == false) {
          this.zonesPone.push({ 'districtName': this.countries[j].districtName, 'districtId': this.countries[j].districtId });
        }
        this.zonearrayP.push(this.countries[j].districtId);
      }
    }
    //console.log('-------------', this.zones);
  }
  GetcityP1(data) {

    this.cityarrayP = [];
    this.CityPone = [];
    for (var j = 0; j < this.countries.length; j++) {
      if (this.countries[j].districtId == data) {
        var foundcity = this.cityarrayP.includes(this.countries[j].cityId);
        if (foundcity == false) {
          this.CityPone.push({ 'cityName': this.countries[j].cityName, 'cityId': this.countries[j].cityId });
        }
        this.cityarrayP.push(this.countries[j].cityId);
      }
    }
    //console.log('-------------', this.CityPone);
  }
  GetZoneP2(data) {

    //alert(data);
    this.CityPtwo = [];
    this.zonearrayP = [];

    this.zonesPtwo = [];
    for (var j = 0; j < this.countries.length; j++) {
      if (this.countries[j].stateId == data) {
        var foundzone = this.zonearrayP.includes(this.countries[j].districtId);
        if (foundzone == false) {
          this.zonesPtwo.push({ 'districtName': this.countries[j].districtName, 'districtId': this.countries[j].districtId });
        }
        this.zonearrayP.push(this.countries[j].districtId);
      }
    }
    //console.log('-------------', this.zones);
  }
  GetcityP2(data) {
    this.CityPtwo = [];
    this.cityarrayP = [];
    for (var j = 0; j < this.countries.length; j++) {
      if (this.countries[j].districtId == data) {
        var foundcity = this.cityarrayP.includes(this.countries[j].cityId);
        if (foundcity == false) {
          this.CityPtwo.push({ 'cityName': this.countries[j].cityName, 'cityId': this.countries[j].cityId });
        }
        this.cityarrayP.push(this.countries[j].cityId);
      }
    }

  }
  GetZoneP3(data) {
    //alert(data);
    this.zonesPthree = [];
    this.zonearrayP = [];
    this.CityPthree = [];
    for (var j = 0; j < this.countries.length; j++) {
      if (this.countries[j].stateId == data) {
        var foundzone = this.zonearrayP.includes(this.countries[j].districtId);
        if (foundzone == false) {
          this.zonesPthree.push({ 'districtName': this.countries[j].districtName, 'districtId': this.countries[j].districtId });
        }
        this.zonearrayP.push(this.countries[j].districtId);
      }
    }
    //console.log('-------------', this.zones);
  }
  GetcityP3(data) {
    this.CityPthree = [];
    this.cityarrayP = []
    for (var j = 0; j < this.countries.length; j++) {
      if (this.countries[j].districtId == data) {
        var foundcity = this.cityarrayP.includes(this.countries[j].cityId);
        if (foundcity == false) {
          this.CityPthree.push({ 'cityName': this.countries[j].cityName, 'cityId': this.countries[j].cityId });
        }
        this.cityarrayP.push(this.countries[j].cityId);
      }
    }

  }

  GetZoneAuth(data) {

    //alert(data);
    this.zonesAuth = [];
    this.CityAuth = [];
    this.zonearrayP = [];
    for (var j = 0; j < this.countries.length; j++) {
      if (this.countries[j].stateId == data) {
        var foundzone = this.zonearrayP.includes(this.countries[j].districtId);
        if (foundzone == false) {
          this.zonesAuth.push({ 'districtName': this.countries[j].districtName, 'districtId': this.countries[j].districtId });
        }
        this.zonearrayP.push(this.countries[j].districtId);
      }
    }
    console.log('-------------', this.zones);
  }
  GetcityAuth(data) {

    this.CityAuth = [];
    for (var j = 0; j < this.countries.length; j++) {
      if (this.countries[j].districtId == data) {
        var foundcity = this.cityarrayP.includes(this.countries[j].cityId);
        if (foundcity == false) {
          this.CityAuth.push({ 'cityName': this.countries[j].cityName, 'cityId': this.countries[j].cityId });
        }
        this.cityarrayP.push(this.countries[j].cityId);
      }
    }

  }

  CheckGst(no) {
debugger;
    this.gsterror = "";
    
    //alert(no);
    var Noarray = no.split("");
    //alert(noarray);
    this.temppan = Noarray[2] + Noarray[3] + Noarray[4] + Noarray[5] + Noarray[6] + Noarray[7] + Noarray[8] + Noarray[9] + Noarray[10] + Noarray[11];
    // alert(temppan);
    var entityPan = this.companNo;
    if (entityPan != this.temppan) {
      //  this.data.alert('Please enter valid GST no','danger');
      this.gsterror = " GST no doesn't match with pan";
    }
    else {
      // this.comState="";
    //   this.Getstate();

      this.CheckGstExistance(this.comgstNo);
    }
    this.CheckeStatecode(this.comState);
    // if (this.comState == "" && this.comgstNo != "") {
    //   this.data.alert('Please select State', 'warning');
    // }
    // else if (this.comState != "" && this.comgstNo != "") {
    //   var stateID = Noarray[0] + Noarray[1];
    //   if (this.comgstNo == stateID) {

    //   }
    //   else {
    //     this.gsterror = "Wrong GST.Please provide proper GST No ."
    //   }
    // }
    //18ALWPG5809L1ZM
    //ALWPG5809L
  }
  CheckGstExistance(gstNo) {
    if (gstNo != undefined && gstNo != '') {
      //console.log(this.loadmail);
      // this.signupObj.email = this.loadmail;
      //  alert(this.signupObj.email);
      if (gstNo != '' && gstNo != undefined) {
        // wip(1);
        //var emailValue = this.comEmail;
        var gstObj = {};
        gstObj['gstNo'] = gstNo;
        var jsonString = JSON.stringify(gstObj);
        this._Service.CheckGst(jsonString)
          .subscribe(response => {
            // wip(0);
            var result = response;
            if (result.error.errorData != '0') {
              // this.data.alert(result.error.errorMsg, 'danger');
              this.data.alert('GST already registered , please try with another GST number', 'warning');
              this.comEmail = '';
            } else {
              //if (result.userResult.checkEmailPhoneFlag == 1) {

              //$('#signupInputEmail').focus();
              // } else {

              // }
            }
          }, reason => {
            // wip(0);
            this.data.alert('Internal Server Error', 'danger')
          });

      }
    } else {
      this.data.alert('Please Provide Email Id', 'warning');
    }

  }
  CheckeStatecode(data) {
    if(data!=""){
      var no = this.comgstNo;
      this.gsterror = "";
      var Noarray = no.split("");
      var StateId = Noarray[0] + Noarray[1];
      if (StateId != data) {
        this.gsterror = "Wrong GST.Please provide proper GST No .";
      }
      else {
        this.gsterror = "";
      }
    }
    else{
      this.gsterror="Please select state.";
      this.data.alert("Please select state.", 'warning');
    }
    
  }
  CheckPan() {
    if (this.companNo != '' && this.companNo != undefined) {
      // wip(1);
      var panValue = this.companNo;
      var panObj = {};
      panObj['panNo'] = panValue;
      var jsonString = JSON.stringify(panObj);
      this._Service.CheckPan(jsonString)
        .subscribe(response => {
          // wip(0);
          var result = response;
          if (result.error.errorData != '0') {
            this.data.alert('Pan No. already registered , please try with another Pan No.', 'warning');
            this.comContact = '';
            // this.data.alert(result.error.error_msg, 'danger');
          } else {
            //if (result.userResult.checkEmailPhoneFlag == 1) {

            //$('#signupInputPhone').focus();
            // } else {

            // }
          }
        }, reason => {
          // wip(0);
          this.data.alert('Internal Server Error', 'danger')

        });
    } else {
      this.data.alert('Please Provide Phone No.', 'warning')
    }
  }

  CheckGstEdit(no) {
    //alert(no);
    var Noarray = no.split("");
    //alert(noarray);
    var temppan = Noarray[2] + Noarray[3] + Noarray[4] + Noarray[5] + Noarray[6] + Noarray[7] + Noarray[8] + Noarray[9] + Noarray[10] + Noarray[11];
    // alert(temppan);
    var entityPan = this.EcompanNo;
    if (entityPan != temppan) {
      this.data.alert('Please enter valid GST no', 'danger');
    }
    else {
      this.CheckGstExistance(this.EcomgstNo);
    }
    //18ALWPG5809L1ZM
    //ALWPG5809L
  }
  checkEmailForCompany() {
    debugger;
    if (this.comEmail != undefined && this.comEmail != '') {
      //console.log(this.loadmail);
      // this.signupObj.email = this.loadmail;
      //  alert(this.signupObj.email);
      if (this.comEmail != '' && this.comEmail != undefined) {
        // wip(1);
        var emailValue = this.comEmail;
        var emailObj = {};
        emailObj['email'] = emailValue;
        var jsonString = JSON.stringify(emailObj);
        this._Service.CheckEmailForCompanyEmail(jsonString)
          .subscribe(response => {
            // wip(0);
            var result = response;
            if (result.error.errorData != '0') {
              // this.data.alert(result.error.errorMsg, 'danger');
              this.data.alert('Email already registered , please try with another email address', 'warning');
              this.comEmail = '';
            } else {
              //if (result.userResult.checkEmailPhoneFlag == 1) {

              //$('#signupInputEmail').focus();
              // } else {

              // }
            }
          }, reason => {
            // wip(0);
            this.data.alert('Internal Server Error', 'danger')
          });

      }
    } else {
      this.data.alert('Please Provide Email Id', 'warning');
    }

  }
  //check phone
  checkPhoneForCompany() {
    debugger;
    if (this.comContact != '' && this.comContact != undefined) {
      // wip(1);
      var phoneValue = this.comContact;
      var phoneObj = {};
      phoneObj['contactNo'] = phoneValue;
      var jsonString = JSON.stringify(phoneObj);

      this._Service.CheckPhoneForConpanyInfo(jsonString)
        .subscribe(response => {
          // wip(0);
          var result = response;
          if (result.error.errorData != '0') {
            this.data.alert('Phone No. already registered , please try with another phone no.', 'warning');
            this.comContact = '';
            // this.data.alert(result.error.error_msg, 'danger');
          } else {
            //if (result.userResult.checkEmailPhoneFlag == 1) {

            //$('#signupInputPhone').focus();
            // } else {

            // }
          }
        }, reason => {
          // wip(0);
          this.data.alert('Internal Server Error', 'danger')

        });
    } else {
      this.data.alert('Please Provide Phone No.', 'warning')
    }

  }
  checkEmailForAuthorization() {
    debugger;
    if (this.aEmailId != undefined && this.aEmailId != '') {
      //console.log(this.loadmail);
      // this.signupObj.email = this.loadmail;
      //  alert(this.signupObj.email);
      if (this.aEmailId != '' && this.aEmailId != undefined) {
        // wip(1);
        var emailValue = this.aEmailId;
        var emailObj = {};
        emailObj['email'] = emailValue;
        var jsonString = JSON.stringify(emailObj);
        this._Service.CheckEmailForAuthorisedEmail(jsonString)
          .subscribe(response => {
            // wip(0);
            var result = response;
            if (result.error.errorData != '0') {
              // this.data.alert(result.error.errorMsg, 'danger');
              this.data.alert('Email already registered , please try with another email address', 'warning');
              this.aEmailId = '';
            } else {
              //  if (result.userResult.checkEmailPhoneFlag == 1) {
              //  this.data.alert('Email already registered , please try with another email address', 'warning');

              //$('#signupInputEmail').focus();
              //  } else {

              //  }
            }
          }, reason => {
            // wip(0);
            this.data.alert('Internal Server Error', 'danger')
          });

      }
    } else {
      this.data.alert('Please Provide Email Id', 'warning');
    }

  }
  //check phone
  checkPhoneForAuthorization() {
    debugger;
    if (this.aContactNo != '' && this.aContactNo != undefined) {
      // wip(1);
      var phoneValue = this.aContactNo;
      var phoneObj = {};
      phoneObj['contactNo'] = phoneValue;
      var jsonString = JSON.stringify(phoneObj);

      this._Service.CheckphoneForAuthorisedInfo(jsonString)
        .subscribe(response => {
          // wip(0);
          var result = response;
          if (result.error.errorData != '0') {
            // this.data.alert(result.error.errorMsg, 'danger');
            this.data.alert('Phone No. already registered , please try with another phone no.', 'warning');
            this.aContactNo = '';
          } else {
            // if (result.userResult.checkEmailPhoneFlag == 1) {

            //$('#signupInputPhone').focus();
            //  } else {

            //  }
          }
        }, reason => {
          // wip(0);
          this.data.alert('Internal Server Error', 'danger')

        });
    } else {
      this.data.alert('Please Provide Phone No.', 'warning')
    }

  }
  handleSelectTab = (identifier) => {

    if (identifier === 'showCompanyInfo') {


      this.showCompanyInfo = true;
      this.showassociationInfo = false;
      this.showPersonalInfo = false;
      this.showaothrizationInfo = false;
      this.showKycInfo = false;
      this.showAuthBankInfo = false;
      this.showMembershipInfo = false;
      this.showTermsCondition = false;
      this.showpreviewInfo = false;


    } else if (identifier === 'showPersonalInfo') {
      // if (this.showCompanyInfo) {
      this.showCompanyInfo = false;
      this.showassociationInfo = false;
      this.showPersonalInfo = true;
      this.showaothrizationInfo = false;
      this.showKycInfo = false;
      this.showAuthBankInfo = false;
      this.showMembershipInfo = false;
      this.showTermsCondition = false;
      this.showpreviewInfo = false;
      // this.showDisclosureInfo = false;
      //}
    } else if (identifier === 'showassociationInfo') {
      //  if (this.showPersonalInfo) {
      this.showCompanyInfo = false;
      this.showassociationInfo = true;
      this.showPersonalInfo = false;
      this.showaothrizationInfo = false;
      this.showKycInfo = false;
      this.showAuthBankInfo = false;
      this.showMembershipInfo = false;
      this.showTermsCondition = false;
      this.showpreviewInfo = false;
      // this.showDisclosureInfo = false;
      // }
      // } else if (identifier === 'showBankInfo') {
      //   if (this.isBankInfoActive) {
      //     this.showApplicantInfo = false;
      //     this.showOfficeAddressInfo = false;
      //     this.showPersonalInfo = false;
      //     this.showBankInfo = true;
      //     this.showAssociationInfo = false;
      //     this.showDisclosureInfo = false;
      //   }
    } else if (identifier === 'showpreviewInfo') {
      //if (this.isAssociationInfoActive) {

      this.showCompanyInfo = false;
      this.showassociationInfo = false;
      this.showPersonalInfo = false;
      this.showaothrizationInfo = false;
      this.showKycInfo = false;
      this.showAuthBankInfo = false;
      this.showMembershipInfo = false;
      this.showTermsCondition = false;
      this.showpreviewInfo = true;
      this.getpreviewInfo();
      // }
    } else if (identifier === 'showaothrizationInfo') {

      // if (this.showassociationInfo||this.showPersonalInfo) {
      this.showCompanyInfo = false;
      this.showassociationInfo = false;
      this.showPersonalInfo = false;
      this.showaothrizationInfo = true;
      this.showKycInfo = false;
      this.showAuthBankInfo = false;
      this.showMembershipInfo = false;
      this.showTermsCondition = false;
      this.showpreviewInfo = false;
      // }
    } else if (identifier === 'showKycInfo') {

      // if (this.showaothrizationInfo) {
      this.showCompanyInfo = false;
      this.showassociationInfo = false;
      this.showPersonalInfo = false;
      this.showaothrizationInfo = false;
      this.showKycInfo = true;
      this.showAuthBankInfo = false;
      this.showMembershipInfo = false;
      this.showTermsCondition = false;
      this.showpreviewInfo = false;
      // }
    } else if (identifier === 'showAuthBankInfo') {

      //  if (this.showKycInfo) {
      this.showCompanyInfo = false;
      this.showassociationInfo = false;
      this.showPersonalInfo = false;
      this.showaothrizationInfo = false;
      this.showKycInfo = false;
      this.showAuthBankInfo = true;
      this.showMembershipInfo = false;
      this.showTermsCondition = false;
      this.showpreviewInfo = false;
      //}
    } else if (identifier === 'showMembershipInfo') {

      // if (this.showAuthBankInfo) {
      this.showCompanyInfo = false;
      this.showassociationInfo = false;
      this.showPersonalInfo = false;
      this.showaothrizationInfo = false;
      this.showKycInfo = false;
      this.showAuthBankInfo = false;
      this.showMembershipInfo = true;
      this.showTermsCondition = false;
      this.showpreviewInfo = false;
      // }
    } else if (identifier === 'showTermsCondition') {


      // if (this.showMembershipInfo) {
      this.showCompanyInfo = false;
      this.showassociationInfo = false;
      this.showPersonalInfo = false;
      this.showaothrizationInfo = false;
      this.showKycInfo = false;
      this.showAuthBankInfo = false;
      this.showMembershipInfo = false;
      this.showTermsCondition = true;
      this.showpreviewInfo = false;
      // }
    }

  }
  getregistrationtype() {
    this._Service.Getregtype()
      .subscribe(data => {

        this.regType = data;

      })
  }
  getbusinesstype() {
    this._Service.getBusinesstype()
      .subscribe(data => {
        this.allbusinessType = data;

      })
  }
  companyInformationdata() {
    debugger;
    var CompanyInfoForm = {};
    // localStorage.setItem('companyId', '10');
    var companyId = localStorage.getItem('companyId');

    CompanyInfoForm['companyId'] = companyId;
    CompanyInfoForm['companyName'] = this.entityName;
    CompanyInfoForm['companyRegTypeId'] = this.entityType;
    CompanyInfoForm['companyWebsite'] = this.websitename;
    CompanyInfoForm['businessTypeId'] = this.tempval;
    CompanyInfoForm['regAddressLineOne'] = this.comAddressone;
    CompanyInfoForm['regAddressLineTwo'] = this.comAddress2;
    CompanyInfoForm['country'] = 1;
    CompanyInfoForm['state'] = this.comState;
    CompanyInfoForm['district'] = this.comDistrict;
    CompanyInfoForm['city'] = this.comCity;
    CompanyInfoForm['pinCode'] = this.comPin;
    CompanyInfoForm['contactNo'] = this.comContact;
    CompanyInfoForm['email'] = this.comEmail;
    CompanyInfoForm['panNo'] = this.companNo;
    var pandate = this.PanIssueDate;
    this.PanIssueDate = pandate.year + '-' + pandate.month + '-' + pandate.day;
    CompanyInfoForm['panIssuedDate'] = this.PanIssueDate;
    CompanyInfoForm['gstNo'] = this.comgstNo;
    var gstdate = this.GstIssueDate;
    this.GstIssueDate = gstdate.year + '-' + gstdate.month + '-' + gstdate.day;
    CompanyInfoForm['gstIssuedDate'] = this.GstIssueDate;
    CompanyInfoForm['iecNo'] = this.comIec;
    CompanyInfoForm['cinNo'] = this.comCin;
    var incdate = this.IncorporationDate;
    // alert(incdate);
    if (incdate != undefined) {
      this.IncorporationDate = incdate.year + '-' + incdate.month + '-' + incdate.day;
      CompanyInfoForm['cinIncorporationDate'] = this.IncorporationDate;
    }
    else {
      CompanyInfoForm['cinIncorporationDate'] = "";
    }


    CompanyInfoForm['corrAddressLineOne'] = this.corrAddressone;
    CompanyInfoForm['corrAddressLineTwo'] = this.corrAddress2;
    CompanyInfoForm['corrCountry'] = 1;
    CompanyInfoForm['corrState'] = this.corrState;
    CompanyInfoForm['corrCity'] = this.corrCity;
    CompanyInfoForm['corrDistrict'] = this.corrDistrict;

    CompanyInfoForm['corrPinCode'] = this.corrPin;
    var entityPan = this.companNo;

    if (this.temppan == this.companNo) {
      if (companyId && this.entityName && this.entityType && this.businessType && this.comAddressone && this.comCountry && this.comState && this.comDistrict && this.comCity && this.comPin && this.comContact && this.comEmail && this.companNo && this.PanIssueDate && this.comgstNo && this.GstIssueDate && this.corrAddressone && this.corrState && this.corrCity && this.corrDistrict && this.corrCity) {
        this.data.alert('Loading...', 'dark');
        this.data.loader = true;
        this._Service.InsertCompanydata(CompanyInfoForm)
          .subscribe(response => {
            if (response.flag == 1) {
              var result = response.userResult;
              var processstep = result.processStep;
              if (processstep == 1) {
                this.handlecompanyInfo();
              }
            }
            else {
              //  alert('Mandatory field missing');
              this.data.alert('Mandatory field missing!', 'danger');
            }
            //  console.log(response);
            // this.data.alert('Successful!', 'danger');

          })
        // 
      }
      else {
        this.data.alert('Mandatory field missing!', 'danger');
      }
    }
    else {
      this.data.alert('Mandatory field missing!', 'danger');

    }


  }
  Copyaddress() {

    this.corrAddressone = this.comAddressone;
    this.corrAddress2 = this.comAddress2;
    this.corrCountry = this.comCountry;
    this.corrState = this.comState;
    this.corrDistrict = this.comDistrict;
    this.corrCity = this.comCity
    this.corrPin = this.comPin;



  }
  PersonalInformationdata() {
    debugger;
    // var personalForm = {};
    // 
    // personalForm['companyId'] = companyId;
    // personalForm['firstName'] = this.pFastname;
    // personalForm['lastName'] = this.pLaststname;
    // personalForm['designation'] = this.Pdesignation;
    // personalForm['contact'] = this.pContactNo;
    // personalForm['email'] = this.pemailId;
    // localStorage.setItem('emailIdFormail',this.pemailId);
    // personalForm['panNo'] = this.PpanNo;
    // personalForm['aadharNo'] = this.PadharNo;
    // var appdate = this.PappointedDate;
    // this.PappointedDate = appdate.year + '-' + appdate.month + '-' + appdate.day;
    // personalForm['appointedDate'] = this.PappointedDate;
    // personalForm['addressLineOne'] = this.pAddressone;
    // personalForm['addressLineTwo'] = this.Paddresstwo;
    // personalForm['country'] = this.pCountry;
    // personalForm['state'] = this.pState;
    // personalForm['city'] = this.pCity;
    // personalForm['district'] = this.Pdistrict;
    // personalForm['pinCode'] = this.PpanNo;

    // this.companyId= localStorage.getItem('companyId');
    if (this.personalInfo[0].appointedDate != '') {
      var appdate = this.personalInfo[0].appointedDate;
      this.personalInfo[0].appointedDate = appdate.year + '-' + appdate.month + '-' + appdate.day
    }
    if (this.personalInfo.length > 1) {
      if (this.personalInfo[1].appointedDate != '') {
        var appdate = this.personalInfo[1].appointedDate;
        this.personalInfo[1].appointedDate = appdate.year + '-' + appdate.month + '-' + appdate.day
      }
    }
    if (this.personalInfo.length > 2) {
      if (this.personalInfo[2].appointedDate != '') {
        var appdate = this.personalInfo[2].appointedDate;
        this.personalInfo[2].appointedDate = appdate.year + '-' + appdate.month + '-' + appdate.day
      }
    }


    //   this.personalInfodata.push(this.personalInfo);
    //this.personalInfodata=[{"companyId":10,"firstName":"xcxvxv","lastName":"sdfsdfsf","designation":"Proprietorship","contact":"242412554","email":"dhriti@gmail.com","panNo":"23523523235","aadharNo":"3424234","appointedDate":"15-1-2021","addessLine1":"sdfsfsf","addressLine2":"sdfsfsf","country":"Albania","state":"617","city":"sdfsdfsd","district":"sdfsfs","pincode":"23523523235"}];
    //console.log('------------------',this.personalInfodata);
    // if (companyId && this.pFastname && this.pLaststname && this.Pdesignation && this.pContactNo && this.pemailId && this.PpanNo && this.PadharNo && this.PappointedDate && this.pAddressone &&
    //   this.pCountry && this.pState && this.pCity && this.Pdistrict && this.PpanNo) {
    this.data.alert('Loading...', 'dark');
    this._Service.InsertPersonaldata(this.personalInfo)
      .subscribe(response => {
        if (response.flag == 1) {
          var result = response.userResult;
          this.data.alert('Successful!', 'success');
          if (result.associationFlag == 'Y') {
            this.handlepersonalInfo();
          }
          else {
            this.handleAssociationInfo();
          }


        }


      })
    // }
    // else {
    //   this.data.alert('Mandatory field missing!', 'danger');
    // }



    // this.handlecompanyInfo();
  }
  AssociationInfodata() {
    //  alert('association');
    //   noOfPersonOwner1;
    // SSNPassportOwner1; CorporateIdOwner1;
    // noOfPersonOwner2;
    // CorporateIdOwner;
    // noOfPersonOwner3;
    // SSNPassportOwner3;
    // CorporateIdOwner2;
    // CorporateIdOwner3;
    // noOfPersonAssociate;
    // SSNPassportAssociate;
    // CorporateIdAssociate;
    // noOfPersonPrincipleOfficer;
    // SSNPassportPrincipleOfficer;
    // SSNPassportOwner2;
    // CorporateIdPrincipleOfficer;
    // noOfPersonEntities;
    // SSNPassportEntities;
    // CorporateIdEntities;
    //   "{
    //     ""companyId"":1223,
    //     ""associationInfoList"" : [
    //         { ""category"": ""aa"",""name"": ""Paybito"",""ssnPassportNo"": ""123"",""corpocateIdNo"": ""saa"" },
    //         { ""category"": ""bb"",""name"": ""vv"",""ssnPassportNo"": ""vv"",""corpocateIdNo"": ""hg"" },
    //          { ""category"": ""cc"",""name"": ""vv"",""ssnPassportNo"": ""vv"",""corpocateIdNo"": ""hg"" }
    //     ]
    // }"

    if (this.noOfPersonOwner1 && this.SSNPassportOwner1 && this.CorporateIdOwner1 && this.noOfPersonOwner2 && this.SSNPassportOwner2 && this.CorporateIdOwner2)
    //  && this.noOfPersonOwner3 && this.SSNPassportOwner3 && this.CorporateIdOwner3 && this.noOfPersonOwner4 && this.SSNPassportOwner4 && this.CorporateIdOwner4 && this.noOfPersonPrincipleOfficer && this.SSNPassportPrincipleOfficer && this.CorporateIdPrincipleOfficer
    // && this.noOfPersonPrincipleOfficer && this.SSNPassportPrincipleOfficer && this.CorporateIdPrincipleOfficer && this.noOfPersonEntities && this.SSNPassportEntities && this.CorporateIdEntities)
    {
      this.data.alert('Loading...', 'dark');
      this.associationInfo = [];
      for (var i = 1; i <= 6; i++) {
        // associationform['companyId']=10;

        var associationform = {};
        associationform['category'] = 'Owners' + i;
        var input, persondata;
        input = document.getElementById("person" + i);
        persondata = input.value;
        associationform['name'] = persondata;
        var input, passpotdata;
        input = document.getElementById("passport" + i);
        passpotdata = input.value;
        associationform['ssnPassportNo'] = passpotdata;
        var input, idata;
        input = document.getElementById("identification" + i);
        idata = input.value;
        associationform['corporateIdNo'] = idata;
        this.associationInfo.push(associationform);
      }

      var associationarray = {};
      associationarray['companyId'] = localStorage.getItem('companyId');;
      associationarray['associationInfoList'] = this.associationInfo;
      this._Service.InsertAssociationdata(associationarray)
        .subscribe(response => {
          if (response.errorData == 0) {
            var result = response;
            this.data.alert('Update Successful!', 'success');

            this.handleAssociationInfo();
          }
          else {
            this.data.alert('Mandatory field missing', 'danger');
          }


        })

    }
    else {

      // alert('Mandatory field missing!');
      this.data.alert('Mandatory field missing!', 'danger');
    }

  }
  AuthorizationInfodata() {
    debugger;
    var aothform = {};
    var cId = localStorage.getItem('companyId');
    aothform['companyId'] = cId;// localStorage.getItem('companyId');
    aothform['firstName'] = this.aFastname;
    aothform['lastName'] = this.aLaststname;
    aothform['designation'] = this.aDesignation;
    aothform['contact'] = this.aContactNo;
    aothform['email'] = this.aEmailId;
    aothform['panNo'] = this.aPanNo;
    aothform['aadharNo'] = this.AadharNumber;
    var adate = this.AappointedDate;
    this.AappointedDate = adate.year + '-' + adate.month + '-' + adate.day;
    aothform['appointedDate'] = this.AappointedDate;
    aothform['addressLineOne'] = this.Aaddressone;
    aothform['addressLineTwo'] = this.Aaddresstwo;
    aothform['country'] = this.Acountry;
    aothform['state'] = this.Astate;
    aothform['city'] = this.Acity;
    aothform['district'] = this.Adistrict;
    aothform['pinCode'] = this.Apincode;
    var aothagrree = this.AuthPolicy;
    if (aothagrree) {
      this.AuthPolicy = 'y';
    }
    else {
      this.AuthPolicy = 'n';
    }

    aothform['readAndAgree'] = this.AuthPolicy;

    // if (cId && this.aFastname && this.aLaststname && this.aDesignation && this.aContactNo && this.aEmailId && this.aPanNo&& this.AadharNumber & this.AappointedDate && this.Aaddressone && this.Acountry && this.Astate && this.Acity && this.Adistrict && this.Apincode&&this.AuthPolicy == 'y') {
    if (cId && this.AuthPolicy == 'y' && this.aEmailId && this.aFastname && this.aLaststname && this.aDesignation && this.Apincode && this.Adistrict && this.AappointedDate && this.Aaddressone && this.Acountry) {
      this.data.alert('Loading...', 'dark');
      this._Service.InsertAothorizationdata(aothform)
        .subscribe(response => {
          if (response.flag == 1) {
            var result = response;
            this.data.alert('Successful!', 'success');

            this.handleAothorizedInfo();
          }
          else {
            // alert('Mandetory field missing');
            this.data.alert('Mandatory field missing', 'danger');
          }


        })
    }
    else {
      //alert('Mandetory field missing');
      this.data.alert('Mandatory field missing', 'success');
    }


  }

  handlBankngInfo() {
    // this.showOfficeAddressInfo = true;
    // this.showApplicantInfo = false;
    //  this.showOfficeAddressInfo = true;
    this.showPersonalInfo = false;
    this.showBankInfo = false;
    this.showAssociationInfo = false;
    this.showDisclosureInfo = false;
    this.showmembershipInfo = false;
    this.bankingInfo = true;
  }
  handlecompanyInfo() {
    this.showCompanyInfo = false;
    this.showPersonalInfo = true;
    this.showassociationInfo = false;
    this.showaothrizationInfo = false;

    this.showKycInfo = false;
    this.showmembershipInfo = false;
    this.showAuthBankInfo = false;
    this.showAssociationInfo = false;

    this.showAssociationInfo = false;

  }
  handlepersonalInfo() {
    this.showCompanyInfo = false;
    this.showPersonalInfo = false;
    this.showassociationInfo = true;
    this.showaothrizationInfo = false;

    this.showKycInfo = false;
    this.showmembershipInfo = false;
    this.showAuthBankInfo = false;
    this.showAssociationInfo = false;

    this.showAssociationInfo = false;


  }
  handleAssociationInfo() {

    this.showCompanyInfo = false;
    this.showPersonalInfo = false;
    this.showassociationInfo = false;
    this.showaothrizationInfo = true;

    this.showKycInfo = false;
    this.showmembershipInfo = false;
    this.showAuthBankInfo = false;
    this.showAssociationInfo = false;

    this.showAssociationInfo = false;
  }
  handleAothorizedInfo() {
    this.showCompanyInfo = false;
    this.showPersonalInfo = false;
    this.showassociationInfo = false;
    this.showaothrizationInfo = false;
    this.showKycInfo = false;
    this.showmembershipInfo = false;
    //this.showBankInfo = false;
    this.showAssociationInfo = false;
    this.showAuthBankInfo = true;
    this.showAssociationInfo = false;
  }
  handlemenbershipInfo(thanksModal) {
    /*  this.showApplicantInfo = false;
     this.showOfficeAddressInfo = false;
     this.showPersonalInfo = false;
     this.showBankInfo = false;
     this.showAssociationInfo = false;
     this.showDisclosureInfo = false;
     this.showKycInfo = false;
     this.showMembershipInfo = false;
     this.showmembershipInfo = false;
     this.showTermsCondition = true; */
    if (this.selectedPackage !== '') {
      // this._route.navigateByUrl('/')
      this.modalService.open(thanksModal, { centered: true, windowClass: 'dark-modal' });

      // this.data.alert('Thanks for registering with us! Your registration is pending for admin verification.', 'success');

    } else {
      alert('Please select a package')
    }
  }
  GoToLoginpage() {
    window.location.href = "http://52.8.99.117/login";
  }
  handletermsndcondition() {

  }

  handlemenbershipInfosecond() {
    // this.showApplicantInfo = false;
    // this.showOfficeAddressInfo = false;
    this.showPersonalInfo = false;
    this.showAuthBankInfo = false;
    this.showAssociationInfo = false;
    this.showDisclosureInfo = false;
    this.showmembershipInfo = false;
    this.showAssociationInfo = false;
    this.showTermsCondition = true
  }

  handleFile = (e, identifier) => {
    let fileArray = e.target.files[0];
    if (fileArray !== undefined) {
      if (identifier === 'corporateArticleOfIncorporation') {
        this.identityProof = e.target.files;

      } else if (identifier === 'corporateTaxIdentification') {
        this.isAssociationInfoActive = e.target.files;

        // } else if (identifier === 'bankReferenceLetter') {
        //   this.bankReferenceLetter = e.target.files;
        // }
      }
    }
  }
  Termcondition_one(content) {
    this.modalService.open(content, { centered: true, windowClass: 'dark-modal' });
  }
  Termcondition_two(contenttwo) {

    this.modalService.open(contenttwo, { centered: true, windowClass: 'dark-modal' });
  }

  addAuthorizedPerson() {
    //debugger;
    //$(function() {
    // $("#addMore").click(function(e) {
    /// e.preventDefault();
    // $("#fieldList").append("<li>&nbsp;</li>");
    // $("#fieldList").append("<li><input type='text' name='name[]' placeholder='Name' /></li>");
    // $("#fieldList").append("<li><input type='text' name='phone[]' placeholder='Phone' /></li>");
    // $("#fieldList").append("<li><input type='text' name='email[]' placeholder='E-Mail' /></li>");
    // });
    // });

    //  var button=document.getElementById("add-user");
    // // button.addEventListener('click', function(event) {
    //     event.preventDefault();
    //     var cln = document.getElementsByClassName("user")[0].cloneNode(true);
    //     var newclan: any;
    //     document.getElementById("users").insertBefore(cln,newclan);
    //     return false;


    var inputs = $('form > button').prev().prev().prevUntil().clone().add('<br><br>');
    $('form > button').on('click', function (e) {
      e.preventDefault();
      $(this).before(inputs.clone());
    });

    // });


  }
  /***** Method defined by @Rupam******/


  /**** method defination for validating KYC  files ****/
  handleValidateKycFiles = (e, identifier) => {
    console.log(e.target.files)
    let fileLength = e.target.files.length;
    if (fileLength > 0) {
      let size = e.target.files[0].size;
      let type = e.target.files[0].type;
      if (identifier === 'identityFile') {
        if (size <= 1048576 && fileLength > 0 &&
          (
            type === 'image/jpeg' || type === 'image/bmp' || type === 'application/pdf' || type === 'application/msword' ||
            type === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'

          )
        ) {
          this.identityFile = e.target.files[0]
          $('#identityFile').siblings('.text-danger').hide()
        } else {
          this.identityFile = ''
          $('#identityFile').siblings('.text-danger').show()
        }
      } else if (identifier === 'companyAddressFile') {
        if (size <= 1048576 && fileLength > 0 &&
          (
            type === 'image/jpeg' || type === 'image/bmp' || type === 'application/pdf' || type === 'application/msword' ||
            type === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'

          )
        ) {
          this.companyAddressFile = e.target.files[0]
          $('#companyAddressFile').siblings('.text-danger').hide()
        } else {
          this.companyAddressFile = ''
          $('#companyAddressFile').siblings('.text-danger').show()
        }

      } else if (identifier === 'cancelChequeFile') {
        if (fileLength > 0
        ) {
          this.cancelChequeFile = e.target.files[0]
          $('#cancelChequeFile').siblings('.text-danger').hide()
        } else {
          this.cancelChequeFile = ''
          $('#cancelChequeFile').siblings('.text-danger').show()
        }

      } else if (identifier === 'authorizedPersonIdentityFile') {
        if (size <= 1048576 && fileLength > 0 &&
          (
            type === 'image/jpeg' || type === 'image/bmp' || type === 'application/pdf' || type === 'application/msword' ||
            type === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'

          )
        ) {
          this.authorizedPersonIdentityFile = e.target.files[0]
          $('#authorizedPersonIdentityFile').siblings('.text-danger').hide()
        } else {
          this.authorizedPersonIdentityFile = ''
          $('#authorizedPersonIdentityFile').siblings('.text-danger').show()
        }

      } else if (identifier === 'authorizedPersonAddressFile') {
        if (size <= 1048576 && fileLength > 0 &&
          (

            type === 'image/jpeg' || type === 'image/bmp' || type === 'application/pdf' || type === 'application/msword' ||
            type === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'

          )
        ) {
          this.authorizedPersonAddressFile = e.target.files[0]
          $('#authorizedPersonAddressFile').siblings('.text-danger').hide()
        } else {
          this.authorizedPersonAddressFile = ''
          $('#authorizedPersonAddressFile').siblings('.text-danger').show()
        }

      } else if (identifier === 'authorizedPersonPicFile') {
        if (size <= 1048576 && fileLength > 0 &&
          (
            type === 'image/jpeg' || type === 'image/bmp' || type === 'application/pdf' || type === 'application/msword' ||
            type === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'

          )
        ) {
          this.authorizedPersonPicFile = e.target.files[0]
          $('#authorizedPersonPicFile').siblings('.text-danger').hide()
        } else {
          this.authorizedPersonPicFile = ''
          $('#authorizedPersonPicFile').siblings('.text-danger').show()
        }

      } else if (identifier === 'authorizedPersonSignatureFile') {
        if (size <= 1048576 && fileLength > 0 &&
          (
            type === 'image/jpeg' || type === 'image/bmp' || type === 'application/pdf' || type === 'application/msword' ||
            type === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'

          )
        ) {
          this.authorizedPersonSignatureFile = e.target.files[0]
          $('#authorizedPersonSignatureFile').siblings('.text-danger').hide()
        } else {
          this.authorizedPersonSignatureFile = ''
          $('#authorizedPersonSignatureFile').siblings('.text-danger').show()
        }

      } else if (identifier === 'authorizedPersonLetterOfauthorityFile') {
        if (size <= 1048576 && fileLength > 0 &&
          (
            type === 'image/jpeg' || type === 'image/bmp' || type === 'application/pdf' || type === 'application/msword' ||
            type === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'

          )
        ) {
          this.authorizedPersonLetterOfauthorityFile = e.target.files[0]
          $('#authorizedPersonLetterOfauthorityFile').siblings('.text-danger').hide()
        } else {
          this.authorizedPersonLetterOfauthorityFile = ''
          $('#authorizedPersonLetterOfauthorityFile').siblings('.text-danger').show()
        }

      } else if (identifier === 'letterOfBank') {
        if (size <= 1048576 &&
          (
            type === 'image/jpeg' || type === 'image/bmp' || type === 'application/pdf' || type === 'application/msword' ||
            type === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'

          )
        ) {
          this.letterOfBank = e.target.files[0]
          $('#letterOfBank').siblings('.text-danger').hide()
        } else {
          this.letterOfBank = ''
          $('#letterOfBank').siblings('.text-danger').show()
        }

      }
    } else {
      $('#' + identifier).siblings('.text-danger').show()
    }
  }
  /**** Method defination for company kyc validation ****/
  validateCompanyKyc = (e, id) => {
    let identifier = '#companyDoc' + id;
    $(identifier).siblings('.text-danger').hide();
    let files = e.target.files;
    let length = e.target.files.length
    let fileArr = [];
    let isValid = false;
    //console.log('************START*************')
    //console.log(this.companyKycDocArray)
    if (length > 0) {
      for (let i = 0; i < files.length; i++) {
        let file = files[i];
        console.log(file)
        if (file.type === 'application/pdf' || file.type === 'application/msword' || file.type === 'image/jpeg' || file.type === 'image/bmp' ||
          file.type === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {

          if (file.size <= 1048576) {
            fileArr.push(file);
            isValid = true
          } else {
            $(identifier).siblings('.text-danger').show();
          }
        } else {
          $(identifier).siblings('.text-danger').show();
        }
      }
      let index = this.companyKycDocArray.findIndex(x => x['id'] === id);
      // console.log(index)
      if (index !== -1) {
        this.companyKycDocArray.splice(index, 1);
      }
    } else {
      $(identifier).siblings('.text-danger').show();
    }
    // console.log(isValid)
    if (isValid) {
      let obj = {}
      obj['id'] = id;
      obj['files'] = fileArr
      this.companyKycDocArray.push(obj);
    }
    // console.log(this.companyKycDocArray)
    //console.log('************END*************')

  }


  /**** Method defination to handle company KYC file upload ****/
  handleCompanyKycFileUpload = (id) => {
    let identifier = 'companyDoc' + id;
    let files: any = (document.getElementById(identifier) as HTMLInputElement).files[0];
    console.log(files);
    let length = files.length;
    if (length === 0) {

    } else {
      let payload = new FormData();
      payload.append('companyId', this.companyId);
      payload.append('kycDocId', id);
      payload.append('kycDocFile', files);

      this.http.post<any>(this.WEBSERVICE + '/files/uploadKycDocs', payload /* { headers: { 'Content-Type': 'multipart/form-data' } } */).subscribe(data => {
        if (data.error.errorData !== 0) {
          this.data.alert(data.error.errorMsg, 'danger');
        } else {
          this.data.alert('Document uploaded successfully', 'success');
        }

      })
    }


  }

  /**** method defination for submitting KYC docs *****/
  handleSubmitKycDoc = () => {
    debugger;
    //console.log(this.companyKycDocArray.length,this.companyKycDocList.length)
    var totalarraylength = this.companyKycDocList.length + this.authCompanyKycIdList.length;
    if (this.companyKycDocArray.length == totalarraylength) {
      this.data.alert('Loading...', 'dark');
      // let payload = new FormData();
      let companyPayload = new FormData();
      companyPayload.append('companyId', this.companyId);
      // payload.append('companyId', this.companyId);

      // payload.append('identityDocId ', this.selectAuthPersonIdentityId);
      // payload.append('identityDoc ', this.authorizedPersonIdentityFile);
      // payload.append('addressDocId', this.selectAuthPersonIdentityAddress);
      // payload.append('addressDoc', this.authorizedPersonAddressFile);
      // payload.append('photoDoc', this.authorizedPersonPicFile);
      // payload.append('signDoc', this.authorizedPersonSignatureFile);
      // payload.append('authLetterDoc', this.authorizedPersonLetterOfauthorityFile);

      let company = this.companyKycDocArray;
      let kycIdStr = '';
      for (let i = 0; i < company.length; i++) {
        kycIdStr += company[i].id + ','
        for (let j = 0; j < company[i].files.length; j++) {
          let file = company[i].files[j]
          companyPayload.append('files', file);
        }
      }
      //console.log(kycIdStr);
      kycIdStr = kycIdStr.slice(0, -1)
      companyPayload.append('kycDocIds', kycIdStr);
      this.data.alert('Loading...', 'dark');
      //   this.http.post<any>(this.WEBSERVICE + '/Files/uploadAuthKycDocs', payload).subscribe(data => {
      //  if (data.error.errorData !== 0) {
      // this.data.alert(data.error.errorMsg, 'danger');
      // } else {

      this.http.post<any>(this.WEBSERVICE + '/files/uploadMultipleCompFiles', companyPayload).subscribe(data => {
        if (data.error.errorData !== 0) {
          this.data.alert(data.error.errorMsg, 'danger');
        } else {
          this.data.alert('Document uploaded successfully', 'success');

          this.showMembershipInfo = false;
          this.showApplicantInfo = false;
          this.showOfficeAddressInfo = false;
          this.showPersonalInfo = false;
          this.showBankInfo = false;
          this.showAssociationInfo = false;
          // this.showDisclosureInfo = true;
          this.showmembershipInfo = false;
          this.showKycInfo = false;
          this.showAuthBankInfo = false;
          this.showTermsCondition = false;
          this.showpreviewInfo = true;
          this.getpreviewInfo();
        }
      })


      ///}

      //  })
      // } else {
      this.data.alert('Please provide all KYC documents', 'danger')
      /*  if (this.authorizedPersonIdentityFile == '' || this.authorizedPersonIdentityFile == undefined) {
         $('#authorizedPersonIdentityFile').siblings('.text-danger').show()
       }
       if (this.authorizedPersonAddressFile == '' || this.authorizedPersonAddressFile == undefined) {
         $('#authorizedPersonAddressFile').siblings('.text-danger').show()
       }
       if (this.authorizedPersonPicFile == '' || this.authorizedPersonPicFile == undefined) {
         $('#authorizedPersonPicFile').siblings('.text-danger').show()
       }
       if (this.authorizedPersonSignatureFile == '' || this.authorizedPersonSignatureFile == undefined) {
         $('#authorizedPersonSignatureFile').siblings('.text-danger').show()
       }
       if (this.authorizedPersonLetterOfauthorityFile == '' || this.authorizedPersonLetterOfauthorityFile == undefined) {
         $('#authorizedPersonLetterOfauthorityFile').siblings('.text-danger').show()
       }
       $('.dynamicFile').each(function () {
         let file = $(this).val();
         if (file === '' || file === undefined) {
           $(this).siblings('.text-danger').show()
         }
       }) */
    }
  }
  /**** method defination for submitting Bank Details *****/
  handleSubmitBankDetails = () => {
    this.accHolderName = (document.getElementById('AccHolderName') as HTMLInputElement).value;
    this.bankName = (document.getElementById('bankName') as HTMLInputElement).value;
    this.bankAccountType = (document.getElementById('bankAccountType') as HTMLInputElement).value;
    this.bankAccountNo = (document.getElementById('bankAccountNo') as HTMLInputElement).value;
    this.bankBranchName = (document.getElementById('bankBranchName') as HTMLInputElement).value;
    this.bankIfscCode = (document.getElementById('bankIfscCode') as HTMLInputElement).value;
    if (
      this.bankName != '' && this.bankAccountType != '' &&
      this.bankAccountNo != '' && this.bankBranchName != '' &&
      this.bankIfscCode != ''
    ) {
      let payload = {
        "companyId": this.companyId,
        "accountHolderName": this.accHolderName,
        "bankName": this.bankName,
        "accountNo": this.bankAccountNo,
        "branchName": this.bankBranchName,
        "ifscCode": this.bankIfscCode,
        "accountType": this.bankAccountType,
      }
      this.data.alert('Loading...', 'dark');
      //this.data.loader = false;/true
      this.http.post<any>(this.WEBSERVICE + '/users/addBankDetails', JSON.stringify(payload), { headers: { 'Content-Type': 'application/json' } }).subscribe(data => {
        if (data.error.errorData !== 0) {
          this.data.alert(data.error.errorMsg, 'danger');
        } else {
          //this.handleSelectTab('showMembershipInfo')
          this.showMembershipInfo = false;
          this.showApplicantInfo = false;
          this.showOfficeAddressInfo = false;
          this.showPersonalInfo = false;
          this.showBankInfo = false;
          this.showAssociationInfo = false;
          // this.showDisclosureInfo = true;
          this.showmembershipInfo = false;
          this.showKycInfo = true;
          this.showAuthBankInfo = false;
          this.showTermsCondition = false;
          /*** Method calling for rendering KYC fields****/
          this.renderKycDocInfo();
          /*** Method calling for rendering KYC fields for authorized person****/
          this.renderAuthPersonKycDocInfo();
          this.renderAuthCompanyKycDocInfo();




        }

      })
    } else {
      this.data.alert('Please provide proper bank information', 'danger');
    }
  }

  /*** Method defination for submitting terms and conditions ****/
  handleSubmitTermsAndCondition = () => {

    if (this.privacyPolicy && this.disclosureAgreement && this.terms) {

      let payload = {
        'companyId': this.companyId,
        'privacyPolicyLink': 1,
        'disclosureAgreement': 1,
        'termsNConditions': 1,
      }
      this.http.post<any>(this.WEBSERVICE + '/users/selectTermsNConditions', JSON.stringify(payload), { headers: { 'Content-Type': 'application/json' } }).subscribe(data => {
        if (data.flag !== 1) {

        } else {
          this.showMembershipInfo = true;
          this.showApplicantInfo = false;
          this.showOfficeAddressInfo = false;
          this.showPersonalInfo = false;
          this.showBankInfo = false;
          this.showAssociationInfo = false;
          // this.showDisclosureInfo = true;
          this.showmembershipInfo = false;
          this.showKycInfo = false;
          this.showAuthBankInfo = false;
          this.showTermsCondition = false;
          this.getAuthorizationEmail();
          this.getAuthorizationEmailforInvoice();
        }

      })
    } else {
      /*  if(!this.privacyPolicy){
         $('#privacyPolicy').css('border-color','red')
       }else  if(!this.disclosureAgreement){
         $('#disclosureAgreement').css('border-color','red')
       }else  if(!this.terms){
         $('#terms').css('border-color','red')
       }  */
      this.termsAndConditionError = "Please agree with Privacy Policy,Disclosure Agreement and Terms and conditions"
    }
  }

  /**** Method defination for company KYC info ****/
  renderKycDocInfo = () => {
    let payload = {
      companyId: this.companyId,
    }
    this.http.post<any>(this.WEBSERVICE + '/users/getCompanyKycDocList', JSON.stringify(payload), { headers: { 'Content-Type': 'application/json' } }).subscribe(data => {
      if (data.error.errorData !== 0) {
        this.data.alert(data.error.errorMsg, 'danger');
      } else {
        this.companyKycDocList = data.companyKycDocList;
        /* if(info !== null){
          for(let i=0;i<info.length;i++){
            let shortName = info[i].documentsShortName;
            if(shortName === "INCORPORATION CERTIFICATE" ){
              this.showCompanyIdDetails = true
            }else if(shortName === "AOA" || shortName === 'MOU' || shortName === 'MOA' || shortName === "ELECTRICITY BILL" ){
              this.showCompanyAddressDetails = true
            }else if(shortName === "CANCEL CHEQUE"){
              this.showCompanyCancelCheckDetails = true
            }else if(shortName === "AUTHORIZATION LETER"){
              this.showCompanyLOBDetails = true
            }
          }
        } */
      }

    })
  }

  /**** Method defination for authorized person KYc info ****/
  renderAuthPersonKycDocInfo = () => {
    let payload = {
      companyId: this.companyId,
    }
    this.http.get<any>(this.WEBSERVICE + '/users/getAuthKycDocList').subscribe(data => {
      if (data.error.errorData !== 0) {
        this.data.alert(data.error.errorMsg, 'danger');
      } else {
        this.authPersonKycIdList = data.companyKycDocList;
      }

    })
  }
  //company auth KYC get method
  renderAuthCompanyKycDocInfo = () => {
    // let payload = {
    //   companyId: this.companyId,
    // }
    this.http.get<any>(this.WEBSERVICE + '/users/getCompanyAuthKycDocList').subscribe(data => {
      if (data.error.errorData !== 0) {
        this.data.alert(data.error.errorMsg, 'danger');
      } else {
        this.authCompanyKycIdList = data.companyKycDocList;
      }

    })
  }
  /**** Method defination for authorized person id details ****/
  handleSelectionForAuthPersonId = (e) => {
    this.selectAuthPersonIdentityId = e.target.value
  }
  /**** Method defination for authorized person id details ****/
  handleSelectionForAuthPersonAddress = (e) => {
    this.selectAuthPersonIdentityAddress = e.target.value
  }

  /*** Method defination for rendering menbership plans ***/
  renderMembershipPlans = () => {
    this.http.get<any>(this.WEBSERVICE + '/users/getMembershipDetails').subscribe(data => {
      /*   if (data.error.errorData !== 0) {
          this.data.alert(data.error.errorMsg, 'danger');
        } else { */
      this.membershipPlanList = data;
      console.log(this.membershipPlanList)
      //}

    })
  }

  /**** method defination for selecting membership plan ****/
  handleSelectMembershipPlan = (e) => {
    this.selectedPackage = e.target.value;
    for (let i = 0; i < this.membershipPlanList.length; i++) {
      let plan = this.membershipPlanList[i];
      if (plan['typeId'] === parseInt(this.selectedPackage)) {
        console.log(plan['typeId'], parseInt(this.selectedPackage))
        this.selectedPlanDesc = plan['description'];
        this.selectedPlanFee = plan['fees'];
        this.selectedPlanDuration = plan['renewalCycle'];
        this.selectedPlanTradingFee = plan['minPgdAmount'];
        this.selectedPlanTradingLimit = plan['minTradeAmount'];
      }
    }

  }
  /**** method defination for generating invoice ****/
  handleGenerateInvoice = (invoiceModal) => {
    let payload = {
      companyId: this.companyId,
      membershipTypeId: this.selectedPackage
    }
    //  alert(this.companyId+'-----------------'+this.selectedPackage)
    // var fd = new FormData();
    // fd.append("companyId", this.companyId);
    // fd.append("membershipTypeId",this.selectedPackage);

    this.http.post<any>(this.WEBSERVICE + '/users/generateAdviceOrInvoice', JSON.stringify(payload), { headers: { 'Content-Type': 'application/json' } }).subscribe(data => {
      if (data.flag === 0) {
        this.data.alert(data.error.errorMsg, 'danger');
      } else {
        debugger;
        let invoice = data.adviceDetails;
        this.invoiceDescription = invoice.adviceDescription;
        this.invoicePurposeCode = invoice.advicePurposeCode;
        this.invoiceMemberShipType = invoice.membershipTypeId;
        this.invoiceBankBeneficiaryName = invoice.adminBankDetails.beneficiaryName;
        this.invoiceAmount = invoice.amount;
        this.invoiceGST = invoice.igstValue;
        this.invoiceBankName = invoice.adminBankDetails.bankName;
        this.invoiceBankAccountNo = invoice.adminBankDetails.accountNo;
        this.invoiceBankIfscCode = invoice.adminBankDetails.ifscCode;
        this.invoiceBankAccountType = invoice.adminBankDetails.accountType;
        this.invoiceId = invoice.adviceId;
        this.modalService.open(invoiceModal, { centered: true });
      }

    })

  }
  /*** Method defination for handling download invoice ***/
  handleDownloadInvoice = () => {
    this.data.alert('Loading...', 'dark');
    //this.data.loader = false;/true
    let payload = {
      companyId: this.companyId,
    }

    /* API call for extracting company details  */
    this.http.post<any>(this.WEBSERVICE + '/users/getCompanyInformation?companyId=' + this.companyId, JSON.stringify(payload), { headers: { 'Content-Type': 'application/json' } }).subscribe(data => {
      /*  if (data.flag === 0) {
         this.data.alert(data.message, 'danger');
       } else { */
      var companyName = data.companyName;
      var companyEmail = data.email;
      //data.email;
      //var companyEmail = 'rupam.hashcash@gmail.com';
      var companyPhone = data.contactNo;
      /* API call for extracting Admin exchange  details  */
      this.http.get<any>(this.WEBSERVICE + '/users/getAdminExchangeDetails').subscribe(data => {
        if (data.error.errorData !== 0) {
          this.data.alert(data.error.errorMsg, 'danger');
        } else {
          var exchangeName = data.adminExchangeDetails.exchangeName;
          var exchangeAddress = data.adminExchangeDetails.address;
          var exchangeEmail = data.adminExchangeDetails.email;
          var exchangeMobile = data.adminExchangeDetails.mobile;

          /* Creating invoice html string  */
          let html = '';
          html += '<html><head></head><body>'
          html += '<style>';
          html += '.invoice-box {';
          html += '    max-width: 800px;';
          html += '    margin: auto;';
          html += '    padding: 30px;';
          html += '    border: 1px solid #eee;';
          html += '    box-shadow: 0 0 10px rgba(0, 0, 0, .15);';
          html += '    font-size: 16px;';
          html += '    line-height: 24px;';
          html += "    font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;";
          html += '    color: #555;}';

          html += '.invoice-box table {';
          html += '    width: 100%;';
          html += '    line-height: inherit;';
          html += '    text-align: left;';
          html += '}';

          html += '.invoice-box table td {';
          html += '    padding: 5px;';
          html += '    vertical-align: top;';
          html += '}';

          html += '.invoice-box table tr td:nth-child(2) {';
          html += '    text-align: right;';
          html += '}';

          html += '.invoice-box table tr.top table td {';
          html += '    padding-bottom: 20px;';
          html += '}';

          html += '.invoice-box table tr.top table td.title {';
          html += '    font-size: 45px;';
          html += '    line-height: 45px;';
          html += '    color: #333;';
          html += '}';

          html += '.invoice-box table tr.information table td {';
          html += '    padding-bottom: 40px;';
          html += '}';

          html += '.invoice-box table tr.heading td {';
          html += '    background: #eee;';
          html += '    border-bottom: 1px solid #ddd;';
          html += '    font-weight: bold;';
          html += '}';

          html += '.invoice-box table tr.details td {';
          html += '    padding-bottom: 20px;';
          html += '}';

          html += '.invoice-box table tr.item td{';
          html += '    border-bottom: 1px solid #eee;';
          html += '}';
          html += '';
          html += '.invoice-box table tr.item.last td {';
          html += '    border-bottom: none;';
          html += '}';

          html += '.invoice-box table tr.total td:nth-child(2) {';
          html += '    border-top: 2px solid #eee;';
          html += '    font-weight: bold;';
          html += '}';

          html += '@media only screen and (max-width: 600px) {';
          html += '    .invoice-box table tr.top table td {';
          html += '        width: 100%;';
          html += '        display: block;';
          html += '        text-align: center;';
          html += '    }';

          html += '    .invoice-box table tr.information table td {';
          html += '        width: 100%;';
          html += '        display: block;';
          html += '        text-align: center;';
          html += '    }';
          html += '}';

          html += '.rtl {';
          html += '    direction: rtl;';
          html += "    font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;";
          html += '}';

          html += ' .rtl table {';
          html += '    text-align: right;';
          html += ' }';

          html += '  .rtl table tr td:nth-child(2) {';
          html += '      text-align: left;';
          html += '  }';
          html += '  </style>';


          html += '<div class="invoice-box">';
          html += '<table cellpadding="0" cellspacing="0">';
          html += '<tr class="top">';
          html += ' <td colspan="2">';
          html += '<table>';
          html += '<tr>';
          html += '<td class="title"> ADVICE';

          html += '</td>';

          html += '<td>Advice #: ' + this.invoiceId;
          // html +='<br>Created: January 1, 2015<br> Due: February 1, 2015';
          // html+='</td>';
          html += ' </tr>';
          html += '</table>';
          html += '</td>';
          html += '</tr>';

          html += '<tr class="information">';
          html += '<td colspan="2">';
          html += '<table>';
          html += '<tr>';
          html += '<td>Company Name : ' + companyName + ' <br> Company Email : ' + companyEmail + '<br> Company Phone : ' + companyPhone;
          html += '</td>';

          html += '<td>   Exchange Name : ' + exchangeName + '<br> Exchange Address :' + exchangeAddress + '<br>Exchange Email : ' + exchangeEmail + '<br>Exchange Phone : ' + exchangeMobile;
          html += '</td>';
          html += '</tr>';
          html += '</table>';
          html += '</td>';
          html += '</tr>';



          html += '<tr class="heading">';
          html += '<td>Item</td>';

          html += ' <td>Price</td>';
          html += '</tr>';

          html += '<tr class="item">';
          html += '<td>';
          html += this.invoiceDescription
          html += '</td>';

          html += '<td>';
          html += parseFloat(this.invoiceAmount).toFixed(2)
          html += '</td>';
          html += '</tr>';



          html += '<tr class="total">';
          html += '<td></td>';


          html += '<td> GST: ' + parseFloat(this.invoiceGST).toFixed(2) + '   </td>';
          html += '</tr>';
          html += '<tr class="total">';
          html += '<td></td>';


          html += '<td> Total: ' + (parseFloat(this.invoiceAmount) + parseFloat(this.invoiceGST)).toFixed(2) + '   </td>';
          html += '</tr>';
          html += '</table>';
          html += '<tr class="information">';
          html += '<td colspan="2">';
          html += '<table>';
          html += '<tr>';
          html += '<td> Bank Name : ' + this.invoiceBankName + '<br> Beneficiary : ' + this.invoiceBankBeneficiaryName + '<br> A/C No : ' + this.invoiceBankAccountNo + ' <br> IFSC : ' + this.invoiceBankIfscCode + ' <br> Type : ' + this.invoiceBankAccountType;
          html += '</td>';

          //html += '<td>Acme Corp.<br> John Doe<br> john@example.com';
          html += '</td>';
          html += '</tr>';
          html += '</table>';
          html += '</td>';
          html += '</tr>';
          html += '</div>';
          html += '</body>';
          console.log(html);



          /* calling API to send invoice via email */
          var emailforAdvice = localStorage.getItem('emailIdFormail');
          let input = {
            "companyId": this.companyId,
            "emailId": emailforAdvice,
            "fileContent": html
          }
          this.http.post<any>(this.WEBSERVICE + '/pdf/createInvoice', JSON.stringify(input), { headers: { 'Content-Type': 'application/json' } }).subscribe(data => {
            if (data.flag === 0) {
              this.data.alert(data.message, 'danger');
            } else {
              this.data.alert('Invoice send to your email', 'success');
            }
          });

          /* calling API to grab user details  */
          let fd = new FormData()
          fd.append('companyId', this.companyId)
          this.http.post<any>(this.WEBSERVICE + '/users/getPreviewDetails', fd).subscribe(data => {
            if (data.flag === 0) {
              this.data.alert(data.message, 'danger');
            } else {
              var company: any = data.companyDetails;
              var bank: any = data.bankDetails;
              var auhorization: any = data.authorizationInfo;
              var personal: any = data.personalDetailsList[0];
              var association: any = data.associInformationList;

              /* generating html for preview details */
              let previewHtml = '';
              previewHtml += '<html><head></head><body>'
              previewHtml += '<h3>Personal Details</h3>'
              previewHtml += '<ul>'
              previewHtml += '<li><b> Name </b> : ' + personal.firstName + '  ' + personal.lastName + ' </li>'
              previewHtml += '<li><b> Designation </b> : ' + personal.designation + ' </li>'
              previewHtml += '<li><b>Contact </b> : ' + personal.contact + ' </li>'
              previewHtml += '<li><b> Email </b> : ' + personal.email + ' </li>'
              previewHtml += '<li><b>Pan No </b> : ' + personal.panNo + ' </li>'
              previewHtml += '<li><b>AadharNo </b> : ' + personal.aadharNo + ' </li>'
              previewHtml += '<li><b>Address Line1 </b> : ' + personal.addressLineOne + ' </li>'
              previewHtml += '<li><b>Address Line2 </b> : ' + personal.addressLineTwo + ' </li>'
              previewHtml += '<li><b> City </b> : ' + personal.city + ' </li>'
              previewHtml += '<li><b> Pincode </b> : ' + personal.pinCode + ' </li>'
              previewHtml += '<li><b> Country </b> : ' + personal.country + ' </li>'
              previewHtml += '<li><b> State </b> : ' + personal.state + ' </li>'
              previewHtml += '<li><b> District </b> : ' + personal.district + ' </li>'
              previewHtml += '</ul>'
              previewHtml += '<h3>Company Details</h3>'
              previewHtml += '<ul>'
              previewHtml += '<li><b> Company Name </b> : ' + company.companyName + ' </li>'
              previewHtml += '<li><b> Business Type </b> : ' + company.businessType + ' </li>'
              previewHtml += '<li><b>Company Website </b> : ' + company.companyWebsite + ' </li>'
              previewHtml += '<li><b>Contact </b> : ' + company.contactNo + ' </li>'
              previewHtml += '<li><b> Email </b> : ' + company.email + ' </li>'
              previewHtml += '<li><b>Pan No </b> : ' + company.panNo + ' </li>'
              previewHtml += '<li><b>Address Line1 </b> : ' + company.corrAddressLineOne + ' </li>'
              previewHtml += '<li><b>Address Line2 </b> : ' + company.corrAddressLineTwo + ' </li>'
              previewHtml += '<li><b> City </b> : ' + company.city + ' </li>'
              previewHtml += '<li><b> Pincode </b> : ' + company.corrPinCode + ' </li>'
              previewHtml += '<li><b> Country </b> : ' + company.country + ' </li>'
              previewHtml += '<li><b> State </b> : ' + company.state + ' </li>'
              previewHtml += '<li><b> District </b> : ' + company.district + ' </li>'
              previewHtml += '<li><b> GST No </b> : ' + company.gstNo + ' </li>'
              previewHtml += '<li><b> CIN No </b> : ' + company.cinNo + ' </li>'
              previewHtml += '</ul>'
              previewHtml += '<h3>Authorization Details</h3>'
              previewHtml += '<ul>'
              previewHtml += '<li><b> Name </b> : ' + auhorization.firstName + ' ' + auhorization.lastName + '</li>'
              previewHtml += '<li><b>Contact </b> : ' + auhorization.contactNo + ' </li>'
              previewHtml += '<li><b> Email </b> : ' + auhorization.email + ' </li>'
              previewHtml += '<li><b>Pan No </b> : ' + auhorization.panNo + ' </li>'
              previewHtml += '<li><b>Address Line1 </b> : ' + auhorization.addressLineOne + ' </li>'
              previewHtml += '<li><b>Address Line2 </b> : ' + auhorization.addressLineTwo + ' </li>'
              previewHtml += '<li><b> City </b> : ' + auhorization.city + ' </li>'
              previewHtml += '<li><b> Pincode </b> : ' + auhorization.pinCode + ' </li>'
              previewHtml += '<li><b> Country </b> : ' + auhorization.country + ' </li>'
              previewHtml += '<li><b> State </b> : ' + auhorization.state + ' </li>'
              previewHtml += '<li><b> District </b> : ' + auhorization.district + ' </li>'
              previewHtml += '</ul>'
              previewHtml += '<h3>Bank Details</h3>'
              previewHtml += '<ul>'
              previewHtml += '<li><b> Bank Name </b> : ' + bank.bankName + '</li>'
              previewHtml += '<li><b>A/C No </b> : ' + bank.accountNo + ' </li>'
              previewHtml += '<li><b>Branch Name </b> : ' + bank.branchName + ' </li>'
              previewHtml += '<li><b> Account Type </b> : ' + bank.accountType + ' </li>'
              previewHtml += '<li><b>IFSC Code </b> : ' + bank.ifscCode + ' </li>'
              previewHtml += '</ul>';

              previewHtml += '<body></html>';

              console.log(previewHtml)

              /* calling API to send user data via email */
              var emailforAdvice = localStorage.getItem('emailIdFormail');
              let prev = {
                "companyId": this.companyId,
                "emailId": emailforAdvice,
                "fileContent": previewHtml
              }
              this.http.post<any>(this.WEBSERVICE + '/pdf/createPreview', JSON.stringify(prev), { headers: { 'Content-Type': 'application/json' } }).subscribe(data => {
                if (data.flag === 0) {
                  this.data.alert(data.message, 'danger');
                } else {
                  this.data.alert('Data send to your email', 'success');
                }
              });
            }
          });
        }
      });
      //}

    })
    this.modalService.dismissAll();
  }
  /**** Method defination for validate bank details ****/
  handleBankValidate = (e, id) => {
    let valueToValidate = e.target.value;
    console.log(valueToValidate)
    console.log(id)
    $('#' + id).siblings('.text-danger2').hide();
    if (id === 'bankAccountNo') {
      if (isNaN(valueToValidate)) {
        console.log('1')
        $('#' + id).val(valueToValidate.slice(0, -1));
      } else if (valueToValidate.length > 16) {
        console.log('2')
        $('#' + id).val(valueToValidate.slice(0, -1));
      }
    } else if (id === 'bankIfscCode') {
      console.log(valueToValidate.match(/[^a-z0-9]/gi))
      if (valueToValidate.match(/[^a-z0-9]/gi)) {
        console.log('1')
        $('#' + id).val(valueToValidate.slice(0, -1));
      }
    }
  }

  /*** Method defination for getting authorization email ***/
  getAuthorizationEmail = () => {
    let payload = {
      companyId: this.companyId,
    }
    this.http.post<any>(this.WEBSERVICE + '/users/getAuthorizationInformation?companyId=' + this.companyId, JSON.stringify(payload), { headers: { 'Content-Type': 'application/json' } }).subscribe(data => {
      if (data.error.errorData !== 0) {
        this.data.alert(data.error.errorMsg, 'danger');
      } else {
        //localStorage.setItem('emailIdFormail', data.personalDetails.email);
      }
    });
  }

  getAuthorizationEmailforInvoice = () => {
    let payload = {
      companyId: this.companyId,
    }
    this.http.post<any>(this.WEBSERVICE + '/users/getAuthorizedPersonInformation?companyId=' + this.companyId, JSON.stringify(payload), { headers: { 'Content-Type': 'application/json' } }).subscribe(data => {
      if (data == '') {
        this.data.alert(data.error.errorMsg, 'danger');
      } else {
        localStorage.setItem('emailIdFormail', data.email);
      }
    });
  }


  getpreviewInfo() {
    this.EbusinessType = [];
    this.getregistrationtype();
    this.getbusinesstype();
    this.Getstate();
    this.EdropdownSettings = {
      singleSelection: false,
      idField: 'businessTypeId',
      textField: 'businessTypeName',
      unSelectAllText: 'UnSelect All',
      enableCheckAll: false,
      itemsShowLimit: 3,
      allowSearchFilter: false
    };
    //debugger;
    var companyId = localStorage.getItem('companyId');

    var fd = new FormData();
    fd.append("companyId", companyId);
    this._Service.GetallInfo(fd)
      .subscribe(data => {
        var response = data;
        this.companydetails = data.companyDetails;
        this.EentityName = this.companydetails.companyName;
        this.EentityType = this.companydetails.companyRegTypeId;
        this.Ewebsitename = this.companydetails.companyWebsite;
        this.EbusinessType = this.companydetails.businessTypes;
        this.EbusinessTypestring = this.companydetails.businessTypeId;
        //var EbusinessTypes = this.companydetails.businessTypeId;
        //var Btemarry = this.companydetails.businessTypeId.split(',');
        // this.EbusinessType = [
        //   { businessTypeId: 2, businessTypeName: "Buyer" },
        //   { businessTypeId: 1, businessTypeName: "Trader" }
        // ];
        console.log('Business type array: ', this.EbusinessType);


        this.EcomAddressone = this.companydetails.regAddressLineOne;
        this.EcomAddress2 = this.companydetails.regAddressLineTwo;
        this.EcomCountry = this.companydetails.country;
        if (this.EcomCountry != null) {
          // setTimeout(() => this.Getstate(this.EcomCountry), 100);

        }
        debugger;
        this.EcomState = this.companydetails.state;
        if (this.EcomState != null) {
          setTimeout(() => this.GetZone(this.EcomState), 100);
        }
        this.Ecomdistrict = this.companydetails.district;
        if (this.Ecomdistrict != null) {

          setTimeout(() => this.Getcity(this.Ecomdistrict), 200);
        }
        this.EcomCity = this.companydetails.city;

        this.EcomPin = this.companydetails.pinCode;
        this.EcomEmail = this.companydetails.email;
        this.EcompanNo = this.companydetails.panNo;
        this.EcomgstNo = this.companydetails.gstNo;
        this.EcomIec = this.companydetails.iecNo;
        this.EcomCin = this.companydetails.cinNo;
        // this.companydetails.panIssuedDate='2021-01-14';
        var pandate = this.companydetails.panIssuedDate;
        var temppdate = pandate.split('-');
        var pyear = parseInt(temppdate[0]);
        var pmonth = parseInt(temppdate[1]);
        var pday = parseInt(temppdate[2]);
        this.EPanNoIssueDate = { year: pyear, month: pmonth, day: pday }//{year: temppdate[0], month: temppdate[1], day:temppdate[2]};

        var gstdate = this.companydetails.panIssuedDate;
        var temppdate = gstdate.split('-');
        var pyear = parseInt(temppdate[0]);
        var pmonth = parseInt(temppdate[1]);
        var pday = parseInt(temppdate[2]);
        this.EGstNoIssueDate = { year: pyear, month: pmonth, day: pday }
        var cindate = this.companydetails.cinIncorporationDate;
        var temppdate = gstdate.split('-');
        var pyear = parseInt(temppdate[0]);
        var pmonth = parseInt(temppdate[1]);
        var pday = parseInt(temppdate[2]);
        this.EInNocorporationDate = { year: pyear, month: pmonth, day: pday }

        this.EcomContact = this.companydetails.contactNo;
        this.EcorrPin = this.companydetails.corrPinCode;
        this.EcorrCity = this.companydetails.corrCity;

        this.EcorrCountry = this.companydetails.corrCountry;
        // if(this.EcorrCountry!=null){
        //   this.Getstate(this.EcorrCountry);
        // }
        this.EcorrState = this.companydetails.corrState;

        if (this.EcorrState != null) {
          //alert(this.EcorrState);
          //setTimeout(() => this.GetZone(this.EcorrState), 100);
        }
        this.EcorrDistrict = this.companydetails.corrDistrict;
        this.EcorrAddressone = this.companydetails.corrAddressLineOne;
        this.corrAddress2 = this.companydetails.corrAddressLineTwo;
        //----company
        this.paersonaldetails = data.personalDetailsList;
        this.EPpincode = this.paersonaldetails[0].pinCode;

        //this.EPdistrict = this.paersonaldetails[0].district;
        //  this.EpCountry = this.paersonaldetails[0].country;

        this.EPaddresstwo = this.paersonaldetails[0].addressLineTwo;
        this.EpAddressone = this.paersonaldetails[0].addressLineOne;
        // this.EPpincode = this.paersonaldetails[0].pincode;
        // this.EpCity = this.paersonaldetails[0].city;

        this.EpCountry = this.paersonaldetails[0].country;
        // if (this.EpCountry != null) {
        //   setTimeout(() => this.Getstate(this.EpCountry), 100);

        // }
        this.EpState = this.paersonaldetails[0].state;
        if (this.EpState != null) {
          // alert(this.EpState);
          setTimeout(() => this.GetZoneP1(this.EpState), 100);
        }
        this.EPdistrict = this.paersonaldetails[0].district;

        if (this.EPdistrict != null) {

          setTimeout(() => this.GetcityP1(this.EPdistrict), 700);
        }

        this.EpCity = this.paersonaldetails[0].city;

        this.EpFastname = this.paersonaldetails[0].firstName;
        this.EpLaststname = this.paersonaldetails[0].lastName;
        this.EPdesignation = this.paersonaldetails[0].designation;
        this.EpContactNo = this.paersonaldetails[0].contact;
        this.EpemailId = this.paersonaldetails[0].email;
        this.EPpanNo = this.paersonaldetails[0].panNo;
        this.EPadharNo = this.paersonaldetails[0].aadharNo;
        var appdate = this.paersonaldetails[0].appointedDate;
        var temppdate = appdate.split('-');
        var pyear = parseInt(temppdate[0]);
        var pmonth = parseInt(temppdate[1]);
        var pday = parseInt(temppdate[2]);
        this.EAppointedDateP = { year: pyear, month: pmonth, day: pday };

        if (this.paersonaldetails[1] != '' && this.paersonaldetails[1] != undefined) {
          document.getElementById('previewpersonaltwo').style.display = 'block';
          this.paersonaldetails = data.personalDetailsList;
          //   this.EPpincodeTwo = this.paersonaldetails[1].pinCode;

          //    this.EpCityTwo = this.paersonaldetails[1].city;

          //  this.EpCountryTwo = this.paersonaldetails[1].country;
          this.EPaddresstwo = this.paersonaldetails[1].addressLineTwo;
          this.EpAddressoneTwo = this.paersonaldetails[1].addressLineOne;
          this.EPpincodeTwo = this.paersonaldetails[1].pinCode;

          //this.EPdistrictTwo = this.paersonaldetails[1].district;
          this.EpCountryTwo = this.paersonaldetails[1].country;
          // if (this.EpCountryTwo != null) {
          // setTimeout(() => this.GetstateP2(this.EpCountryTwo), 100);

          // }
          this.EpStateTwo = this.paersonaldetails[1].state;
          if (this.EpStateTwo != null) {
            //alert(this.EpStateTwo);
            setTimeout(() => this.GetZoneP2(this.EpStateTwo), 700);
          }
          this.EcomDistrictTwo = this.paersonaldetails[1].district;
          if (this.EcomDistrictTwo != null) {

            setTimeout(() => this.GetcityP2(this.EcomDistrictTwo), 900);
          }

          this.EpCityTwo = this.paersonaldetails[1].city;
          this.EpFastnameTwo = this.paersonaldetails[1].firstName;
          this.EpLaststnameTwo = this.paersonaldetails[1].lastName;
          this.EPdesignationTwo = this.paersonaldetails[1].designation;
          this.EpContactNoTwo = this.paersonaldetails[1].contact;
          this.EpemailIdTwo = this.paersonaldetails[1].email;
          this.EPpanNoTwo = this.paersonaldetails[1].panNo;
          this.EPadharNoTwo = this.paersonaldetails[1].aadharNo;
          var appdate = this.paersonaldetails[1].appointedDate;
          var temppdate = appdate.split('-');
          var pyear = parseInt(temppdate[0]);
          var pmonth = parseInt(temppdate[1]);
          var pday = parseInt(temppdate[2]);
          this.EAppointedDatePTwo = { year: pyear, month: pmonth, day: pday }
        }
        if (this.paersonaldetails.length = 3) {
          // document.getElementById('previewpersonalthree').style.display = 'none';
          // this.paersonaldetails = data.personalDetailsList;
          // this.EPpincodeThree = this.paersonaldetails[1].pinCode;
          // this.EpCityThree = this.paersonaldetails[1].city;
          // this.EcomDistrictThree = this.paersonaldetails[1].district;
          // this.EpCountryThree = this.paersonaldetails[1].country;
          // this.EpStateThree = this.paersonaldetails[1].state;
          // this.EPaddresstwoThree = this.paersonaldetails[1].addressLineTwo;
          // this.EpAddressoneThree = this.paersonaldetails[1].addressLineOne;
          // this.EPpincodeThree = this.paersonaldetails[1].pincode;
          // this.EpCityThree = this.paersonaldetails[1].city;
          // //this.EPdistrictTwo = this.paersonaldetails[1].district;
          // this.EpCountryThree = this.paersonaldetails[1].country;
          // this.EpFastnameThree = this.paersonaldetails[1].firstName;
          // this.EpLaststnameThree = this.paersonaldetails[1].lastName;
          // this.EPdesignationThree = this.paersonaldetails[1].designation;
          // this.EpContactNoThree = this.paersonaldetails[1].contact;
          // this.EpemailIdThree = this.paersonaldetails[1].email;
          // this.EPpanNoThree = this.paersonaldetails[1].panNo;
          // this.EPadharNoThree = this.paersonaldetails[1].aadharNo;
          // var appdate = this.paersonaldetails[1].appointedDate;
          // var temppdate = appdate.split('-');
          // var pyear = parseInt(temppdate[0]);
          // var pmonth = parseInt(temppdate[1]);
          // var pday = parseInt(temppdate[2]);
          // this.EAppointedDatePThree = { year: pyear, month: pmonth, day: pday }
        }
        // this.EPappointedDate = this.paersonaldetails[0].appointedDate;

        //this.EpersonalInfodata = [];
        //-------------end personal;
        debugger;
        this.authorizationInfo = data.authorizationInfo;
        this.AuthId = this.authorizationInfo.id;
        localStorage.setItem('pInfoID', this.AuthId);
        this.EaFastname = this.authorizationInfo.firstName;
        this.EaLaststname = this.authorizationInfo.lastName;
        //this.EAdistrict = this.authorizationInfo.district;
        this.EaDesignation = this.authorizationInfo.designation;
        // if(this.EaDesignation=='COMPLIANCEOFFICER '){

        // }
        this.EaPanNo = this.authorizationInfo.panNo;
        this.EAadharNumber = this.authorizationInfo.aadharNo;
        this.EApincode = this.authorizationInfo.pincode;
        this.EaContactNo = this.authorizationInfo.contact;
        this.EaEmailId = this.authorizationInfo.email;

        var cindate = this.authorizationInfo.appointedDate;
        var temppdate = cindate.split('-');
        var pyear = parseInt(temppdate[0]);
        var pmonth = parseInt(temppdate[1]);
        var pday = parseInt(temppdate[2]);
        this.EAappointedDate = { year: pyear, month: pmonth, day: pday };
        this.EAaddressone = this.authorizationInfo.addressLineOne;
        this.EAaddresstwo = this.authorizationInfo.addressLineTwo;
        this.EAcountry = this.authorizationInfo.country;
        if (this.EAcountry != null) {
          //setTimeout(() => this.Getstate(this.EAcountry), 100);
        }
        this.EAstate = this.authorizationInfo.state;
        if (this.EAstate != null) {
          setTimeout(() => this.GetZoneAuth(this.EAstate), 100);
        }
        // if(this.Acountry!=null){
        //   this.Getstate(this.Acountry);
        // }
        // this.Astate = this.authorizationInfo.state;

        this.EAdistrict = this.authorizationInfo.district;
        // alert(this.EAdistrict);
        if (this.EAdistrict != null) {
          setTimeout(() => this.GetcityAuth(this.EAdistrict), 100);
        }
        this.EAcity = this.authorizationInfo.city;
        // alert(this.EAcity);
        this.EApincode = this.authorizationInfo.pinCode;
        this.EAcountry = this.authorizationInfo.country;


        //--------------------
        if (data.associInformationList != null) {
          this.association = data.associInformationList;
          this.EAid = this.association[0].id;

          this.EAssocicategoty = this.association[0].category;
          this.EAssociname = this.association[0].name;
          this.EAssocipassport = this.association[0].ssnPassportNo;
          this.EAssocicorporateId = this.association[0].corporateIdNo;
          this.EAssociationEdit = false;
          this.EAssociationSubmit = false;
        }
        else {
          document.getElementById('shadow-box').style.display = 'none';
          this.EAssociationEdit = true;
          this.EAssociationSubmit = true;
        }

        //----------------


        this.bankdetails = data.bankDetails;
        this.bankId = this.bankdetails.companyBankId;
        this.EBankName = this.bankdetails.bankName;
        this.EBankAccountType = this.bankdetails.accountType;
        this.EbankBranchName = this.bankdetails.branchName;
        this.EbankIfscCode = this.bankdetails.ifscCode;

        this.EBankAccountNo = this.bankdetails.accountNo;
        console.log('**********************', this.bankdetails);

      })

  }
  UpdateCompanydetails() {

    var CompanyUpdateInfoForm = {};
    // localStorage.setItem('companyId', '30');
    var companyId = localStorage.getItem('companyId');
    CompanyUpdateInfoForm['companyId'] = companyId;
    CompanyUpdateInfoForm['companyName'] = this.EentityName;
    CompanyUpdateInfoForm['companyRegTypeId'] = this.EentityType;
    CompanyUpdateInfoForm['companyWebsite'] = this.Ewebsitename;
    CompanyUpdateInfoForm['businessTypeId'] = this.tempval;
    CompanyUpdateInfoForm['regAddressLineOne'] = this.EcomAddressone;
    CompanyUpdateInfoForm['regAddressLineTwo'] = this.EcomAddress2;
    CompanyUpdateInfoForm['country'] = this.EcomCountry;
    CompanyUpdateInfoForm['state'] = this.EcomState;
    CompanyUpdateInfoForm['district'] = this.Ecomdistrict;
    CompanyUpdateInfoForm['city'] = this.EcomCity;
    CompanyUpdateInfoForm['pinCode'] = this.EcomPin;
    CompanyUpdateInfoForm['contactNo'] = this.EcomContact;
    CompanyUpdateInfoForm['email'] = this.EcomEmail;
    CompanyUpdateInfoForm['panNo'] = this.EcompanNo;
    var pandate = this.EPanNoIssueDate;
    this.EPanIssueDate = pandate.year + '-' + pandate.month + '-' + pandate.day;
    CompanyUpdateInfoForm['panIssuedDate'] = this.EPanIssueDate;
    CompanyUpdateInfoForm['gstNo'] = this.EcomgstNo;
    var gstdate = this.EGstNoIssueDate;
    this.EGstIssueDate = gstdate.year + '-' + gstdate.month + '-' + gstdate.day;
    CompanyUpdateInfoForm['gstIssuedDate'] = this.EGstIssueDate;
    CompanyUpdateInfoForm['iecNo'] = this.EcomIec;
    CompanyUpdateInfoForm['cinNo'] = this.EcomCin;
    var incdate = this.EInNocorporationDate;
    this.EIncorporationDate = incdate.year + '-' + incdate.month + '-' + incdate.day;
    CompanyUpdateInfoForm['cinIncorporationDate'] = this.EIncorporationDate;
    CompanyUpdateInfoForm['corrAddressLineOne'] = this.EcorrAddressone;
    CompanyUpdateInfoForm['corrAddressLineTwo'] = this.EcorrAddress2;
    CompanyUpdateInfoForm['corrCountry'] = this.EcorrCountry;
    CompanyUpdateInfoForm['corrState'] = this.EcorrState;
    CompanyUpdateInfoForm['corrCity'] = this.EcorrCity;
    CompanyUpdateInfoForm['corrDistrict'] = this.EcorrDistrict;

    CompanyUpdateInfoForm['corrPinCode'] = this.EcorrCity;

    if (this.tempval == '') {

      this.tempval = this.EbusinessTypestring;
      CompanyUpdateInfoForm['businessTypeId'] = this.tempval;
    }
    // CompanyUpdateInfoForm={"companyId":30,"companyName":"asdasdasd","companyRegTypeId":"4","companyWebsite":"asdasdasda","businessType":"On Hold","regAddress1":"asdasdasdf","regAddress2":"sdfsfsf","country":"Algeria","state":"1108","district":"asdfasfas","city":"asdasda","pinCode":"234534","contactNo":"3245252352","email":"dhriti@gmail.com","panNo":"2352525252","panIssuedDate":"7-1-2021","gstNo":"35325235235","gstIssuedDate":"31-12-2020","iecNo":"ewrwerwerwer","cinNo":"53345353543","cinIncorporationDate":"15-1-2021","corrAddressLine1":"vvv","corrAddressLine2":"vvvd","corrCountry":"wrwrw","corrState":"gdgdg","corrDistrict":"wewew","corrCity":"dfdf","corrPinCode":"vcvcv"};

    // if (companyId && this.entityName && this.entityType && this.businessType && this.comAddressone && this.comCountry && this.comState && this.comDistrict && this.comCity && this.comPin && this.comContact && this.comEmail && this.companNo && this.PanIssueDate && this.comgstNo && this.GstIssueDate && this.IncorporationDate
    //   && this.corrAddressone && this.corrAddress2 && this.corrState && this.corrCity && this.corrDistrict && this.corrCity) {
    this._Service.Updatecompanydetails(CompanyUpdateInfoForm)
      .subscribe(response => {
        if (response.error.errorData == 0) {
          var result = response.userResult;
          this.data.alert('Update Successful!', 'success');
          this.getpreviewInfo();
          //  alert('Update Successful!');

        }
        else {
          this.data.alert('Mandatory field missing!', 'danger');
          // alert('Mandatory field missing');
        }
        //  console.log(response);
        // this.data.alert('Successful!', 'danger');

      })
    // 
  }
  Updatepersonaldetails() {

    this.personalUpdateInfodata = [];
    var personalUpdateForm = {};
    var companyId = localStorage.getItem('companyId');
    personalUpdateForm['companyId'] = companyId;
    var pId = localStorage.getItem('pInfoID');
    personalUpdateForm['id'] = pId;
    personalUpdateForm['firstName'] = this.EpFastname;
    personalUpdateForm['lastName'] = this.EpLaststname;
    personalUpdateForm['designation'] = this.EPdesignation;
    personalUpdateForm['contact'] = this.EpContactNo;
    personalUpdateForm['email'] = this.EpemailId;
    personalUpdateForm['panNo'] = this.EPpanNo;
    personalUpdateForm['aadharNo'] = this.EPadharNo;
    var appdate = this.EAppointedDateP;
    //  this.EPappointedDate = appdate.day + '-' + appdate.month + '-' + appdate.year;
    personalUpdateForm['appointedDate'] = appdate.day + '-' + appdate.month + '-' + appdate.year;
    personalUpdateForm['addressLineOne'] = this.EpAddressone;
    personalUpdateForm['addressLineTwo'] = this.EPaddresstwo;
    personalUpdateForm['country'] = this.EpCountry;
    personalUpdateForm['state'] = this.EpState;
    personalUpdateForm['city'] = this.EpCity;
    personalUpdateForm['district'] = this.EPdistrict;
    personalUpdateForm['pinCode'] = this.EPpanNo;
    this.personalUpdateInfodata.push(personalUpdateForm);
    if (this.paersonaldetails[1] != '' && this.paersonaldetails[1] != undefined) {
      var personalUpdatetwoForm = {};
      var companyId = localStorage.getItem('companyId');
      personalUpdatetwoForm['companyId'] = companyId;
      var pId = localStorage.getItem('pInfoID');
      personalUpdatetwoForm['id'] = pId;
      personalUpdatetwoForm['firstName'] = this.EpFastnameTwo;
      personalUpdatetwoForm['lastName'] = this.EpLaststnameTwo;
      personalUpdatetwoForm['designation'] = this.EPdesignationTwo;
      personalUpdatetwoForm['contact'] = this.EpContactNoTwo;
      personalUpdatetwoForm['email'] = this.EpemailIdTwo;
      personalUpdatetwoForm['panNo'] = this.EPpanNoTwo;
      personalUpdatetwoForm['aadharNo'] = this.EPadharNoTwo;
      var appdate = this.EAppointedDatePTwo;
      //  this.EPappointedDate = appdate.day + '-' + appdate.month + '-' + appdate.year;
      personalUpdatetwoForm['appointedDate'] = appdate.day + '-' + appdate.month + '-' + appdate.year;
      personalUpdatetwoForm['addressLineOne'] = this.EpAddressoneTwo;
      personalUpdatetwoForm['addressLineTwo'] = this.EPaddresstwoTwo;
      personalUpdatetwoForm['country'] = this.EpCountryTwo;
      personalUpdatetwoForm['state'] = this.EpStateTwo;
      personalUpdatetwoForm['city'] = this.EpCityTwo;
      personalUpdatetwoForm['district'] = this.EcomDistrictTwo;
      personalUpdatetwoForm['pinCode'] = this.EPpanNoTwo;
      this.personalUpdateInfodata.push(personalUpdatetwoForm);
    }

    //this.personalInfodata=[{"companyId":10,"firstName":"xcxvxv","lastName":"sdfsdfsf","designation":"Proprietorship","contact":"242412554","email":"dhriti@gmail.com","panNo":"23523523235","aadharNo":"3424234","appointedDate":"15-1-2021","addessLine1":"sdfsfsf","addressLine2":"sdfsfsf","country":"Albania","state":"617","city":"sdfsdfsd","district":"sdfsfs","pincode":"23523523235"}];
    //console.log('------------------',this.personalInfodata);
    if (companyId && this.EpFastname && this.EpLaststname && this.EPdesignation && this.EpContactNo && this.EpemailId && this.EPpanNo && this.EPadharNo && this.EAppointedDateP && this.EpAddressone &&
      this.EpCountry && this.EpState && this.EpCity && this.EPdistrict && this.EPpanNo) {
      this._Service.Updatepersonaldetails(this.personalUpdateInfodata)
        .subscribe(response => {
          if (response.error.errorData != 1) {
            var result = response.userResult;

            alert('Update Successful!');
            this.getpreviewInfo();
            this.data.alert('Successful!', 'success');



          }
          else {
            this.data.alert('Mandatory field missing!', 'danger');
            alert('Mandatory field missing!');
          }


        })
    }
    else {
      alert('Mandatory field missing!');
      this.data.alert('Mandatory field missing!', 'danger');
    }

  }
  // else {
  //   this.data.alert('Mandatory field missing!', 'danger');
  // }
  UpdateAssociation() {
    var UpdateassocInfoForm = {};
    //[{"id":"97","companyId":"30","category": "aa","name": "Paybito","ssnPassportNo": "123","corpocateIdNo": "saa" }]
    //[{"id":97,"companyId":"30","category":"aa","name":"Paybito","ssnPassportNo":"123","corpocateIdNo":"346346346"}]
    UpdateassocInfoForm['id'] = this.EAid;
    var companyId = localStorage.getItem('companyId');
    UpdateassocInfoForm['companyId'] = companyId;
    UpdateassocInfoForm['category'] = this.EAssocicategoty;
    UpdateassocInfoForm['name'] = this.EAssociname;
    UpdateassocInfoForm['ssnPassportNo'] = this.EAssocipassport;
    UpdateassocInfoForm['corporateIdNo'] = this.EAssocicorporateId;
    this.updateAssociation.push(UpdateassocInfoForm);
    this._Service.Updateassociationdetails(this.updateAssociation)
      .subscribe(response => {
        if (response.error.errorData == 0) {
          var result = response.userResult;
          this.getpreviewInfo();
          alert('Update Successful!');

          this.data.alert('Update Successful!', 'success');
          this.previewAssociation = true;
        }
        else {
          this.data.alert('Mandatory field missing', 'danger')
          alert('Mandatory field missing!');
        }


      })
  }

  UpdateAothorization() {
    debugger;
    var aothUpdateform = {};
    var cId = localStorage.getItem('companyId');

    aothUpdateform['companyId'] = cId;
    aothUpdateform['id'] = this.AuthId;// localStorage.getItem('companyId');
    aothUpdateform['firstName'] = this.EaFastname;
    aothUpdateform['lastName'] = this.EaLaststname;
    aothUpdateform['designation'] = this.EaDesignation;
    aothUpdateform['contact'] = this.EaContactNo;
    aothUpdateform['email'] = this.EaEmailId;
    aothUpdateform['panNo'] = this.EaPanNo;
    aothUpdateform['aadharNo'] = this.EAadharNumber;
    // var appdate = this.EAppointedDate;
    // this.EPappointedDate = appdate.day + '-' + appdate.month + '-' + appdate.year;
    // personalUpdateForm['appointedDate'] = this.EPappointedDate;
    var adate = this.EAappointedDate;
    //this.EAappointedDate = adate.year + '-' + adate.month + '-' + adate.day;
    aothUpdateform['appointedDate'] = adate.year + '-' + adate.month + '-' + adate.day;
    aothUpdateform['addressLineOne'] = this.EAaddressone;
    aothUpdateform['addressLineTwo'] = this.EAaddresstwo;
    aothUpdateform['country'] = this.EAcountry;
    aothUpdateform['state'] = this.EAstate;
    aothUpdateform['city'] = this.EAcity;
    aothUpdateform['district'] = this.EAdistrict;
    aothUpdateform['pinCode'] = this.EApincode;

    //aothform['readNAgree'] = this.AuthPolicy;

    // if (cId && this.aFastname && this.aLaststname && this.aDesignation && this.aContactNo && this.aEmailId && this.aPanNo&& this.AadharNumber & this.AappointedDate && this.Aaddressone && this.Acountry && this.Astate && this.Acity && this.Adistrict && this.Apincode&&this.AuthPolicy == 'y') {
    if (cId && this.AuthId && this.EaFastname && this.EaLaststname && this.EaEmailId && this.EaDesignation && this.EaPanNo && this.EAappointedDate && this.EApincode && this.EAdistrict && this.EAcountry && this.EAaddressone) {

      this._Service.Updateaauthorizationdetails(aothUpdateform)
        .subscribe(response => {
          if (response.error.errorData == 0) {
            var result = response;
            alert('Update Successful!');
            this.data.alert('Update Successful!!', 'success');

            // this.handleAothorizedInfo();
          }
          else {
            alert('Mandetory field missing');
            this.data.alert('Mandatory field missing', 'danger');
          }


        })
    }
    else {
      alert('Mandetory field missing');
      this.data.alert('Mandatory field missing', 'success');
    }




  }
  UpdatebankInfo() {
    debugger;
    //{"companyBankId":"17","companyId":2,"bankName":"aaa","accountNo":"45625","branchName":"Asz","ifscCode":"Awe12","accountType":"saving"
    //public EBankName: any;
    // public EBankAccountType: any = '';
    // public EbankBranchName: any;
    // public EbankIfscCode: any;

    // public EBankAccountNo: any;
    var bankInfoUpdateform = {};
    bankInfoUpdateform['companyBankId'] = this.bankId;
    var cId = localStorage.getItem('companyId');

    bankInfoUpdateform['companyId'] = cId;
    bankInfoUpdateform['accountHolderName'] = this.EaccHolderName;
    bankInfoUpdateform['bankName'] = this.EbankBranchName;
    bankInfoUpdateform['accountNo'] = this.EBankAccountNo;
    bankInfoUpdateform['branchName'] = this.EbankBranchName;
    bankInfoUpdateform['ifscCode'] = this.EbankIfscCode;
    bankInfoUpdateform['accountType'] = this.EBankAccountType;
    if (cId && this.bankId && this.EbankBranchName && this.EBankAccountNo && this.EbankBranchName && this.EbankIfscCode && this.EBankAccountType) {

      this._Service.Updatebankdetails(bankInfoUpdateform)
        .subscribe(response => {
          if (response.error.errorData == 0) {
            var result = response;
            alert('Update Successful!');
            this.data.alert('Update Successful!!', 'success');
            this.getpreviewInfo();
            // this.handleAothorizedInfo();
          }
          else {
            alert('Mandetory field missing');
            this.data.alert('Mandatory field missing', 'danger');
          }


        })

    }
    else {
      this.data.alert('Mandatory field missing', 'danger');
      alert('Mandetory field missing');
    }
  }

  EditCompanyInfo() {
    // document.getElementById('asad').style. = false;
    this.previewone = false;
    this.previewtwo = false;
    this.previewthree = false;
    this.previewfour = false;
//this.previewfive = false;
//this.previewsix = false;
    this.previewseven = false;
    this.previeweight = false;
    this.previewnine = false;
    this.previewten = false;
    this.previeweleven = false;
    this.previewtwelve = false;
    this.previewthirteen = false;
    this.previewfourteen = false;
    this.previewfifteen = false;
    //this.previewsixteen = false;
//this.previewseventeen = false;
    this.previeweighteen = false;
    this.previewnineteen = false;
    this.previewtwenty = false;
    this.previewtwentyone = false;
    this.previewtwentytwo = false;
    this.previewtwentythree = false;
    this.previewtwentyfour = false;
    this.previewtwentyfive = false;

    // document.getElementById('EuserName').removeAttribute('disabled');
    // document.getElementById('EcCompanyType').removeAttribute('disabled');
    // document.getElementById('Ebtype').removeAttribute('disabled');
    // document.getElementById('Ecin').removeAttribute('disabled');
    this.CindatePickerDisabled = false;
    // document.getElementById('Ecind').removeAttribute('disabled');
    // document.getElementById('EpanNo').removeAttribute('disabled');
    // document.getElementById('Epand').removeAttribute('disabled');
    this.PandatePickerDisabled = false;
    //// document.getElementById('Egstin').removeAttribute('disabled');
    // document.getElementById('Ecdp').removeAttribute('disabled');
    this.datePickerDisabled = false;
    //  document.getElementById('BEcdp').removeAttribute('disabled');

    //    document.getElementById('Eiec').removeAttribute('disabled');
    // document.getElementById('EregAddress1').removeAttribute('disabled');
    // document.getElementById('EregAddress2').removeAttribute('disabled');
    // document.getElementById('EcCountry').removeAttribute('disabled');
    // document.getElementById('Eregstate').removeAttribute('disabled');
    // document.getElementById('comdistrict').removeAttribute('disabled');
    // document.getElementById('EcCity').removeAttribute('disabled');
    // document.getElementById('Econ').removeAttribute('disabled');
    // document.getElementById('Eemail').removeAttribute('disabled');
    // document.getElementById('EcompanyWebsite').removeAttribute('disabled');
    // document.getElementById('Epin').removeAttribute('disabled');


    // document.getElementById('EcorregAddress1').removeAttribute('disabled');
    // document.getElementById('EcorrregAddress2').removeAttribute('disabled');
    // document.getElementById('ECcCountry').removeAttribute('disabled');
    // document.getElementById('Ecorregstate').removeAttribute('disabled');
    // document.getElementById('Ecorcomdistrict').removeAttribute('disabled');
    // document.getElementById('ECorcCity').removeAttribute('disabled');
    // document.getElementById('Ecorpin').removeAttribute('disabled');



  }
  EditpersonalInfo() {
    // document.getElementById('EpfName').removeAttribute('disabled');
    // document.getElementById('Eplname').removeAttribute('disabled');
    // document.getElementById('Epdesig').removeAttribute('disabled');
    // document.getElementById('EpCnum').removeAttribute('disabled');
    // document.getElementById('Epemail').removeAttribute('disabled');
    // document.getElementById('EPPanNo').removeAttribute('disabled');
    // document.getElementById('EpAdharno').removeAttribute('disabled');
    //  document.getElementById('Edp').removeAttribute('disabled');
    // document.getElementById('EpAdone').removeAttribute('disabled');
    // document.getElementById('EpAddtwo').removeAttribute('disabled');
    // document.getElementById('ErCountry').removeAttribute('disabled');
    // document.getElementById('EPregstate').removeAttribute('disabled');
    // document.getElementById('Epdistrict').removeAttribute('disabled');
    // document.getElementById('Epcity').removeAttribute('disabled');
    // document.getElementById('EPpin').removeAttribute('disabled');
    this.previewPersonalone = false;
    this.previewPersonaltwo = false;
    this.previewPersonalthree = false;
    this.previewPersonalfour = false;
    this.previewPersonalfive = false;
    this.previewPersonalsix = false;
    this.previewPersonalseven = false;
    this.previewPersonaleight = false;
    this.previewPersonalnine = false;
    this.previewPersonalten = false;
    this.previewPersonaleleven = false;
    this.previewPersonaltwelve = false;
    this.previewPersonalthirteen = false;
    this.EAppointedDateper = false;
    this.previewPersonalonearrayone = false;
    //-------------

  }
  EditAssociationInfo() {
    this.previewAssociation = false;
    // document.getElementById('EAssocategoty').removeAttribute('disabled');
    // document.getElementById('EAssoname').removeAttribute('disabled');
    // document.getElementById('EAssopassport').removeAttribute('disabled');
    // document.getElementById('EAssocorporateId').removeAttribute('disabled');
  }
  EditAothorizationInfo() {
    //  alert('------------');
    this.datePickerDisabledAuth = false;
    this.previewauthone = false;
    this.previewauthtwo = false;
    this.previewauththree = false;
    // this.previewauthfour = false;
    // this.previewauthfive = false;
    this.previewauthsix = false;
    this.previewauthseven = false;
    this.previewautheight = false;
    this.previewauthnine = false;
    this.previewauthten = false;
    this.previewautheleven = false;
    this.previewauthtwelve = false;
    this.previewauththirteen = false;
    this.previewauthfourteen = false;
    // document.getElementById('EafName').removeAttribute('disabled');
    // document.getElementById('Ealname').removeAttribute('disabled');
    // document.getElementById('Eadesig').removeAttribute('disabled');
    // document.getElementById('EaCnum').removeAttribute('disabled');
    // document.getElementById('Eaemail').removeAttribute('disabled');
    // document.getElementById('EaPan').removeAttribute('disabled');
    // document.getElementById('EaAdharno').removeAttribute('disabled');
    // document.getElementById('EAdp').removeAttribute('disabled');
    // document.getElementById('EAdone').removeAttribute('disabled');
    // document.getElementById('EAddtwo').removeAttribute('disabled');
    // document.getElementById('EregCountry').removeAttribute('disabled');
    // document.getElementById('EAgstate').removeAttribute('disabled');
    // document.getElementById('Eadistrict').removeAttribute('disabled');
    // document.getElementById('Eacity').removeAttribute('disabled');
    // document.getElementById('Eapin').removeAttribute('disabled');

  }
  EditbankInfo() {

    this.previewbankone = false;
    this.previewbanktwo = false;
    this.previewbankthree = false;
    this.previewbankfour = false;
  }
  GotoTandC() {
    this.showCompanyInfo = false;
    this.showassociationInfo = false;
    this.showPersonalInfo = false;
    this.showaothrizationInfo = false;
    this.showKycInfo = false;
    this.showAuthBankInfo = false;
    this.showMembershipInfo = false;
    this.showTermsCondition = true;
    this.showpreviewInfo = false;
  }
}


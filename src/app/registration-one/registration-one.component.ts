import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CoreDataService } from '../core-data.service';
import * as $ from 'jquery';
import {
  Router
} from '@angular/router';
import { ServiceoneService } from '../services/serviceone.service';
@Component({
  selector: 'app-registration-one',
  templateUrl: './registration-one.component.html',
  styleUrls: ['./registration-one.component.css']
})
export class RegistrationOneComponent implements OnInit {
  public companyName = "";
  public CompanyEmail = "";
  public fullname: any = "";
  public phone: any = "";
  public errormessage: any;
  constructor(private _Service: ServiceoneService, private http: HttpClient, private data: CoreDataService, private route: Router) { }


  ngOnInit(): void {
    localStorage.clear();
  }
  goTootpPage() {
    var userobj = {};
    userobj['name'] = this.fullname;
    userobj['tempPhNo'] = this.phone;
    userobj['companyName'] = this.companyName;
    userobj['email'] = this.CompanyEmail;
    if (this.fullname != "" && this.phone != ""&& this.CompanyEmail!=""&&this.companyName!="") {
      this._Service.GetOtp(userobj)
        .subscribe(response => {

          var result = response;
          if (result.flag = 1) {
            var value = result.userResult;
            localStorage.setItem('tmpMobileno', this.phone);
            localStorage.setItem('tmpOtp', value.phNoOtp);
            this.route.navigateByUrl('/Signupotp');
          }
          else {
            this.errormessage = result.message;
          }
        })
    }
    else {
      this.errormessage = " Mandatory field missing.";
    }


  }
 
}

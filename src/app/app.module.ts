import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AngularDraggableModule } from 'angular2-draggable';
import { FormsModule }   from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { NavbarComponent } from './navbar/navbar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon'; 
import {MatMenuModule} from '@angular/material/menu';
import { LayoutComponent } from './layout/layout.component';
import {MatButtonModule} from '@angular/material/button';
// import{DialogOverviewExampleDialog} from '../app/navbar/navbar.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSelectModule} from '@angular/material/select';
import { ReactiveFormsModule } from '@angular/forms';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatTabsModule} from '@angular/material/tabs';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
// import { NgbdSortableHeader } from './demosor/demosor.component';
import { TvChartContainerComponent } from './tv-chart-container/tv-chart-container.component';
import{CoreDataService} from '../app/core-data.service';
import { HttpClientModule } from "@angular/common/http";
import { LoaderComponent } from './loader/loader.component';
import { QRCodeModule } from 'angularx-qrcode';
import { BuystoplimitdierctiveDirectiveDirective } from './directives/buystoplimitdierctive-directive.directive';
import { ForgetpasswordComponent } from './forgetpassword/forgetpassword.component';
import { SecuretokenComponent } from './securetoken/securetoken.component';
import { RegistrationOneComponent } from './registration-one/registration-one.component';
import { SignupotpComponent } from './signupotp/signupotp.component';
import { SignupformComponent } from './signupform/signupform.component';
import { SidenavComponent } from './sidenav/sidenav.component';
import { AccountinfoComponent } from './accountinfo/accountinfo.component';
import { DatePipe } from '@angular/common';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavbarComponent,
    LayoutComponent,
   
    
    
    TvChartContainerComponent,
    
    LoaderComponent,
   
    BuystoplimitdierctiveDirectiveDirective,
    ForgetpasswordComponent,
    SecuretokenComponent,
    RegistrationOneComponent,
    SignupotpComponent,
    SignupformComponent,
    SidenavComponent,
    AccountinfoComponent
 
   
  ],
  imports: [MatExpansionModule,ReactiveFormsModule ,MatSelectModule,
    MatFormFieldModule,
    MatInputModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    QRCodeModule,
    FormsModule,
    NgMultiSelectDropDownModule.forRoot(),

    BrowserAnimationsModule,MatToolbarModule,MatIconModule,MatMenuModule,MatButtonModule,AngularDraggableModule,MatDialogModule,MatTabsModule, NgbModule
  ],
  exports: [MatExpansionModule,MatSelectModule,MatFormFieldModule,MatToolbarModule,MatIconModule,MatToolbarModule,MatMenuModule,MatButtonModule,MatDialogModule,MatTabsModule],
  entryComponents:[],
  providers: [TvChartContainerComponent,CoreDataService,DatePipe],
  bootstrap: [AppComponent],
  
})
export class AppModule { }

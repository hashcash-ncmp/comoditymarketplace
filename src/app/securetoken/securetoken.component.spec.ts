import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecuretokenComponent } from './securetoken.component';

describe('SecuretokenComponent', () => {
  let component: SecuretokenComponent;
  let fixture: ComponentFixture<SecuretokenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecuretokenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecuretokenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

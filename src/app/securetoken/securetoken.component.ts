import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CoreDataService } from '../core-data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-securetoken',
  templateUrl: './securetoken.component.html',
  styleUrls: ['./securetoken.component.css']
})
export class SecuretokenComponent implements OnInit {

  constructor(private http:HttpClient, private data:CoreDataService, private route:Router) { }
  signupObj:any;
  email: any;
  ngOnInit(): void {
  }
  secureToken(isValid){
    if(isValid){
      var tokenObj={};
      tokenObj['email']=this.email;
      var jsonString=JSON.stringify(tokenObj);
      // wip(1);
      //login webservice
      this.http.post<any>(this.data.WEBSERVICE+'/user/ForgotPassword',jsonString,{headers: {
          'Content-Type': 'application/json'
        }})
     .subscribe(response=>{
        // wip(0);

        var result=response;
        if(result.error.error_data!='0'){
          this.data.alert(result.error.error_msg,'danger');
        }else{
        this.data.alert('Secure Token sent to registered email','success');
        this.route.navigateByUrl('/forgetpassword');
        }

      },function(reason){
        // wip(0);
        this.data.alert('Internal Server Error','danger')
      });
    }else{
      this.data.alert('Please provide valid email','warning');
    }
  }
}

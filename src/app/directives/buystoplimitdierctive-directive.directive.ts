//import { Directive, ElementRef, HostListener, Input } from '@angular/core';
import { Directive, ElementRef, HostListener, Input, forwardRef, Attribute, AfterContentInit, OnChanges, OnDestroy } from '@angular/core';
import { NG_VALIDATORS, AbstractControl, ValidatorFn, Validator, FormControl, NgModel } from '@angular/forms';

@Directive({
  selector: '[appBuystoplimitdierctive]'
})

export class BuystoplimitdierctiveDirectiveDirective {

  private regex: RegExp = new RegExp(/^\d*\.?\d{0,2}$/g);
  @Input() limit = 8;
  private specialKeys: Array<string> = ['Backspace', 'Tab', 'End', 'Home', '-', 'ArrowLeft', 'ArrowRight', 'Del', 'Delete'];

  constructor(
    private el: ElementRef
  ) { }

  @HostListener('keydown', ['$event'])
  onKeyDown(event: KeyboardEvent) {

    // Allow Backspace, tab, end, and home keys
    if (this.specialKeys.indexOf(event.key) !== -1) {
      return;
    }

    let current: string = this.el.nativeElement.value;
    const position = this.el.nativeElement.selectionStart;
    const next: string = [current.slice(0, position), event.key == 'Decimal' ? '.' : event.key, current.slice(position)].join('');
    // 
    // console.log('dhriti----------',next);
    let abcd = next.split(".")[1];

  //  console.log('dhriti----------', abcd);
    const digits = (num, count = 0) => {
      if (num) {
        return digits(Math.floor(num / 10), ++count);
      };
      return count;
    };
    console.log('dhriti----------',digits(abcd));

    if (next && digits(abcd) >= this.limit) {
      event.preventDefault();
    }

    // if (next && !String(next).match(this.regex)) {
    //   event.preventDefault();
    // }

    // if(abcd && abcd.length>this.limit){
    //   event.preventDefault();
    // }
    // if (next && !String(next).match(this.regex)) {
    //   event.preventDefault();
    // }
    // }

  }
}
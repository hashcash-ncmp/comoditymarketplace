import { Component, OnInit, Inject,DoCheck, ViewContainerRef, ComponentFactoryResolver } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
// import { OrderbookComponent } from '../orderbook/orderbook.component';
// import { MarketwatchComponent } from "../marketwatch/marketwatch.component";
// import { ChartoneComponent } from "../chartone/chartone.component";
// import { OrdershortcutComponent } from '../ordershortcut/ordershortcut.component'
// import{TickerComponent} from "../ticker/ticker.component";
// import { CharttwoComponent } from "../charttwo/charttwo.component";
// import { DemosorComponent } from "../demosor/demosor.component";
// import { TvChartContainerComponent } from "../tv-chart-container/tv-chart-container.component";
// import{AnnouncementComponent}from "../announcement/announcement.component";
import * as $ from "jquery";
import { from } from 'rxjs';
import { FormControl, Validators } from '@angular/forms';
import { NgForm } from '@angular/forms';
import { BLACK_ON_WHITE_CSS_CLASS } from '@angular/cdk/a11y/high-contrast-mode/high-contrast-mode-detector';
import { CoreDataService } from '../core-data.service';
// import { MarketdataModule } from '../marketdata/marketdata.module';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Subscription, timer } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DeviceDetectorService } from 'ngx-device-detector';
import { UUID } from 'angular2-uuid';
import { CookieService } from 'ngx-cookie-service';
//import{PaybitoService} from '../services/paybito.service';
import { ActivatedRoute } from '@angular/router';

export interface DialogData {
  animal: string;
  name: string;
}
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})

export class NavbarComponent implements OnInit {
  animal: string;
  name: string;
  loaded = false;
  myComponent?: any;
  username: any;
  shorcutdatafinal:any;
  errormessage:any;
  loader: boolean;
  userID:any;
  // Email:string;
  // public Password:any;

  // demo_html = require('!!html-loader!./navbar.component.html');
  // demo_ts = require('!!raw-loader!./navbar.component.ts');
  // demo_scss = require('!!raw-loader!./navbar.component.css');


  constructor(private http:HttpClient,private route1: ActivatedRoute,public _CoreDataService: CoreDataService,private viewContainerRef: ViewContainerRef, private cfr: ComponentFactoryResolver, public data: CoreDataService) { }

  ngOnInit(): void {
  //   this._PaybitoService.count.subscribe(c=>{
  //   var result=c;
  // console.log('ttttttttttttttttttttttttt',c);
  




  //   })

    const firstParam: string = this.route1.snapshot.queryParamMap.get('userId');
    const secondParam: string = this.route1.snapshot.queryParamMap.get('location');
    const thirdParam: string = this.route1.snapshot.queryParamMap.get('deviceId');

    if (firstParam != null && secondParam != null && thirdParam != null) {
      var deviceinfoObj = {};
      deviceinfoObj['userId'] = firstParam;
      deviceinfoObj['location'] = secondParam;
      deviceinfoObj['deviceId'] = thirdParam;
      var jsonString = JSON.stringify(deviceinfoObj);
      this.http.post<any>(this._CoreDataService.WEBSERVICE + '/user/deviceVerification', jsonString, { headers: { 'content-Type': 'application/json' } })
        .subscribe(response => {
          console.log(response)
          if (response.error.error_data == '1') {
            // this.error = true;
            // this.loader = false;
            this.errormessage = response.error.error_msg;
            this._CoreDataService.alert(this.errormessage, 'warning');
          }
          else {
            // this.error = true;
            // this.loader = false;
            this.errormessage = response.error.error_msg;
            this._CoreDataService.alert(this.errormessage, 'success');
          }
        })
    }

    if (firstParam != null && secondParam == null) {
      var userblockObj = {};
      userblockObj['userId'] = firstParam;
      var jsonString = JSON.stringify(userblockObj);
      this.http.post<any>(this._CoreDataService.WEBSERVICE + '/user/blockAccount', jsonString, { headers: { 'content-Type': 'application/json' } })
        .subscribe(response => {
          console.log(response)
          if (response.error.error_data == '1') {
           // this.error = true;
        //    this.loader = false;
            this.errormessage = response.error.error_msg;
            this._CoreDataService.alert(this.errormessage, 'warning');
          }
          else {
         //   this.error = true;
            this.loader = false;
            this.errormessage = response.error.error_msg;
            this._CoreDataService.alert(this.errormessage, 'success');
          }
        })
    }
     this.userID=localStorage.getItem('user_id');
    this.username = localStorage.getItem('user_name');
  //  alert(this.username);
 // this.animate()
//  this._TickerComponent.getdata();
//  this.shorcutdatafinal=this._TickerComponent.shortcutdata;shortcutdatapaybito
//this.shorcutdatafinal=this._TickerComponent.shortcutdatapaybito;


 
  }
// nextCount() {
//     this._PaybitoService.nextCount();
//   }
  // async load() {
  //   this.viewContainerRef.clear();
  //   const { TickerComponent } = await import('../ticker/ticker.component');
  //   this.viewContainerRef.createComponent(
  //     this.cfr.resolveComponentFactory(TickerComponent)
  //   );

  // }
  animate(){
  
      var block_text = $('.ticker li').map(function() { return $(this).html();}).toArray();
      $(".ticker").html("<p>" + block_text + "</p>");
      var ticker_text = $('.ticker p');
      var ticker_width = $(".ticker").width();
      var text_x = ticker_width;
    
    var  scroll_ticker = function() {
        text_x--;
        ticker_text.css("left", text_x);
        if (text_x < -1 * ticker_text.width()) {
          text_x = ticker_width;
        }
      }
      
      setInterval(scroll_ticker, 10);
    
    
  }
  
  logout(){
  // alert('a');
    //  document.getElementById('log-view').style.visibility='block';
    //  document.getElementById('logout-view').style.visibility='none';
    this.data.alert('Logout Successful!', 'danger');
    localStorage.clear();
  }
  ngDoCheck() {
    this.username = localStorage.getItem('username');
    this.userID=localStorage.getItem('user_id');//'1939';  
  }
  // Opendatawindow(){


  // }
  Openwatchlist() {
    $("#daataTab").style.display = 'block';
  }

}
